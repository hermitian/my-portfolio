# My Portfolio

This repository contains a selection of my work. It is still being put together, thus so far only a few items are included:

- [ ] Theses for my diploma and my masters degree
- [ ] The presentation for a talk I gave at the DPG Spring Meeting, regarding my masters thesis
- [ ] A project for automated processing of evaluation data
- [ ] My solution to the Advent of Code challenges, ongoing.
