"""
Advent of code 2023, day 14
"""
import os
import time

import numpy as np

###############################################################################


def read_file_as_array(filepath: str) -> np.ndarray:
    """
    Reads the given file (with absolute path) into a 2d array with each
    character as an element of the inner array.

    Parameters
    ----------
    filepath: str
        The absolute path to the target file.

    Returns
    -------
    np.char.asarray
        The lines of the file as a list of lists, each character being an
        element of the inner lists.

    Raises
    ------
    None
    """
    input_data: list[list[str]] = []
    with open(filepath, "r", encoding="utf-8") as input_file:
        for line in input_file:
            line_content: str = line.rstrip()
            line_array: list[str] = list(line_content)
            input_data.append(line_array)

    return np.array(input_data)


###############################################################################


def get_weight_of_rolled_boulders(unrolled_array: np.ndarray) -> int:
    """
    Takes a grid of boulders and rolls all round boulder towards north.

    Parameters
    ----------
    unrolled_array: np.ndarray
        The original array of unrolled boulders.

    Returns
    -------
    np.ndarray
        The array after rolling boulders towards north.

    Raises
    ------
    None
    """
    weight_in_columns: list[int] = []
    length_of_column: int = unrolled_array.shape[1]

    for column_iter in unrolled_array.T:
        furthest_boulder: int = 0
        column_weight: int = 0
        for row_counter, row_iter in enumerate(column_iter):
            if row_iter == "#":
                furthest_boulder = row_counter + 1
            if row_iter == "O":
                column_weight += length_of_column - (furthest_boulder)
                furthest_boulder += 1
        weight_in_columns.append(column_weight)

    return sum(weight_in_columns)


###############################################################################


def tilt_antenna(grid_original: np.ndarray, tilt_direction: str) -> np.ndarray:
    """
    Takes the grid and a direction, and moves all moveable objects on the grid
    as far in the given direction as possible.

    Parameters
    ----------
    grid_original: np.ndarray
        The grid on which to perform the tilt operation.
    tilt_direction: str
        The cardinal direction (N, W, E, S) in which to tilt.

    Returns
    -------
    np.ndarray
        The original grid after the tilt operation.

    Raises
    ------
    None
    """
    ###########################################################################
    # Methods
    ###########################################################################

    def prepare_grid(grid_target: np.ndarray, direction: str) -> np.ndarray:
        grid_prepared: np.ndarray = np.zeros(grid_target.shape)
        if direction == "N":
            grid_prepared = np.transpose(grid_target)
        elif direction == "S":
            grid_prepared = np.fliplr(np.transpose(grid_target))
        elif direction == "W":
            grid_prepared = grid_target
        elif direction == "E":
            grid_prepared = np.fliplr(grid_target)
        else:
            print(f"Tilt direction {direction} not recognized")
            raise ValueError

        return grid_prepared

    ###########################################################################

    def roll_grid(grid_target: np.ndarray) -> np.ndarray:
        rolled_grid: list[np.ndarray] = []
        for row_iter in grid_target:
            rolled_row: np.ndarray = np.empty(row_iter.shape, dtype="str")
            last_blocking_column_no: int = -1
            for entry_no, entry_iter in enumerate(row_iter):
                if entry_iter == "#":
                    last_blocking_column_no = entry_no
                    rolled_row[entry_no] = "#"
                elif entry_iter == "O":
                    last_blocking_column_no += 1
                    rolled_row[entry_no] = "."
                    rolled_row[last_blocking_column_no] = "O"
                elif entry_iter == ".":
                    rolled_row[entry_no] = "."

            rolled_grid.append(rolled_row)

        return np.array(rolled_grid)

    ###########################################################################

    def postprocess(grid_target: np.ndarray, direction: str) -> np.ndarray:
        grid_prepared: np.ndarray = np.zeros(grid_target.shape)
        if direction == "N":
            grid_prepared = np.transpose(grid_target)
        elif direction == "S":
            grid_prepared = np.transpose(np.fliplr(grid_target))
        elif direction == "W":
            grid_prepared = grid_target
        elif direction == "E":
            grid_prepared = np.fliplr(grid_target)
        else:
            print(f"Tilt direction {direction} not recognized")
            raise ValueError

        return grid_prepared

    ###########################################################################
    # Execution
    ###########################################################################

    grid_prepared: np.ndarray = prepare_grid(
        np.array(grid_original), tilt_direction
    )
    grid_rolled: np.ndarray = roll_grid(grid_prepared)

    grid_final: np.ndarray = postprocess(grid_rolled, tilt_direction)

    return grid_final


###############################################################################


def get_longest_repeating_pattern(weight_list: list[int]) -> list[int]:
    """
    TODO
    """

    def check_following_entry_match(
        pattern_source: int, match_target: int, pattern_stop: int
    ) -> list[int] | None:
        if (
            match_target >= len(weight_list)
            or weight_list[pattern_source] != weight_list[match_target]
            or pattern_source >= pattern_stop
        ):
            return None

        subpattern_from_here: list[int] = [weight_list[pattern_source]]
        subpattern_from_next: list[int] | None = check_following_entry_match(
            pattern_source + 1, match_target + 1, pattern_stop
        )

        if subpattern_from_next:
            subpattern_from_here.extend(subpattern_from_next)

        return subpattern_from_here

    longest_pattern: list[int] = []

    for pattern_start_iter in range(len(weight_list)):
        current_pattern: list[int] | None = []

        for match_start_iter in range(
            pattern_start_iter + 1, len(weight_list)
        ):
            current_pattern = check_following_entry_match(
                pattern_start_iter, match_start_iter, match_start_iter
            )

            if current_pattern and len(current_pattern) > len(longest_pattern):
                longest_pattern = current_pattern

    return longest_pattern


###############################################################################


def roll_cycle(grid_target: np.ndarray) -> np.ndarray:
    """
    Takes the grid and moves everything north, west, south, and east, in this
    order.

    Parameters
    ----------
    grid_target: np.ndarray
        The grid on which to perform the roll cycle.

    Returns
    -------
    np.ndarray
        The original grid after the roll cycle.

    Raises
    ------
    None
    """
    grid_rolled: np.ndarray = tilt_antenna(grid_target, "N")
    grid_rolled = tilt_antenna(grid_rolled, "W")
    grid_rolled = tilt_antenna(grid_rolled, "S")
    grid_rolled = tilt_antenna(grid_rolled, "E")

    return grid_rolled


###############################################################################


def main() -> None:
    """
    Runs the whole script.
    """
    # Performance
    time_start: float = time.perf_counter()

    # Parsing of file
    input_data: np.ndarray = read_file_as_array(os.getcwd() + "/input_day_14")

    # 14-A: Find total weight on north support
    boulders_weight_total: int = get_weight_of_rolled_boulders(input_data)
    print(f"Total boulder weight on north support: {boulders_weight_total}")

    # 14-B: Get total weigth after 1e9 rolls in each direction
    # TODO
    # print(input_data)
    # weight_after_cycle: list[int] = [get_weight_of_rolled_boulders(input_data)]
    # rolled_boulder: np.ndarray = input_data
    # for roll_cycle_counter in range(10000):
    #     rolled_boulder = roll_cycle(rolled_boulder)
    #     roll_cycle_counter += 1
    #     boulders_weight_total_cycled: int = get_weight_of_rolled_boulders(
    #         rolled_boulder
    #     )
    #     weight_after_cycle.append(boulders_weight_total_cycled)
    #     minimum_weight: int = min(weight_after_cycle)
    #     longest_pattern: list[int] = get_longest_repeating_pattern(
    #         weight_after_cycle
    #     )
    #     print(
    #         f"Total boulder weight on north support after {roll_cycle_counter}"
    #         + f" roll cycles: {boulders_weight_total_cycled}. Minimum weight: "
    #         + f"{minimum_weight}."
    #         + f"Longest pattern: {longest_pattern} of length "
    #         + f"{len(longest_pattern)}",
    #         end="\r",
    #     )

    # Performance
    time_end: float = time.perf_counter() - time_start
    print(f"{time_end=:.3f}")


main()
