"""
Advent of code 2023, day 11
"""
import itertools
import os
import time

import numpy as np


def read_file_as_array(filepath: str) -> np.ndarray:
    """
    Reads the given file (with absolute path) into a 2d array with each
    character as an element of the inner array.

    Parameters
    ----------
    filepath: str
        The absolute path to the target file.

    Returns
    -------
    list[list[str]]
        The lines of the file as a list of lists, each character being an
        element of the inner lists.

    Raises
    ------
    None
    """
    input_data: list[list[str]] = []
    with open(filepath, "r", encoding="utf-8") as input_file:
        for line in input_file:
            line_content: str = line.rstrip()
            line_array: list[str] = list(line_content)
            input_data.append(line_array)

    return np.array(input_data)


###############################################################################


def find_galaxies(grid: np.ndarray) -> list[tuple]:
    """
    Takes a grid and finds the coordinates of all galaxies ("#").

    Parameters
    ----------
    grid: np.ndarray
        The 2d-grid with galaxies and empty tiles.

    Returns
    -------
    list[tuple[int, int]]
        A list of coordinates of all galaxies in the grid.

    Raises
    ------
    None
    """
    galaxies: list[tuple] = []
    coord_iter: np.nditer = np.nditer(grid, flags=["multi_index"])
    for element_iter in coord_iter:
        if element_iter == "#":
            galaxy_coords: tuple = coord_iter.multi_index
            galaxies.append(galaxy_coords)

    return galaxies


###############################################################################


def find_empty_lines(grid: np.ndarray) -> tuple[list[int], list[int]]:
    """
    Takes a grid and finds all rows and columns with no galaxy ("#") in them.

    Parameters
    ----------
    grid: np.ndarray
        The 2d-grid with galaxies and empty tiles.

    Returns
    -------
    list[int]
        A list of rows in which no galaxy was found.
    list[int]
        A list of columns in which no galaxy was found.

    Raises
    ------
    None
    """
    empty_rows: list[int] = []
    for row_iter in range(grid.shape[0]):
        if "#" not in grid[row_iter]:
            empty_rows.append(row_iter)

    empty_columns: list[int] = []
    for column_iter in range(grid.shape[1]):
        if "#" not in grid[:, column_iter]:
            empty_columns.append(column_iter)

    return empty_rows, empty_columns


###############################################################################


def expand_universe(
    galaxies: list[tuple],
    empty_rows: list[int],
    empty_columns: list[int],
    expansion: int = 1,
) -> list[tuple[int, int]]:
    """
    Takes a list of galaxy coordinates and two lists of empty rows and columns
    and adjusts each galaxy's coordinates to the expanded universe.

    Parameters
    ----------
    galaxies: list[tuple[int, int]]
        The list of galaxy coordinates.
    empty_rows: list[int]
        List of list indices that represent empty rows in the grid.
    empty_columns: list[int]
        List of list indices that represent empty columns in the grid.
    expansion: int, default = 1
        The number of lines to insert at each empty line.

    Returns
    -------
    list[tuple[int, int]]
        The list of adjusted galaxy coordinates.

    Raises
    ------
    None
    """
    galaxies_adjusted: list[tuple[int, int]] = []

    for galaxy_iter in galaxies:
        # Adjust row coordinates, axis=0
        galaxy_row: int = galaxy_iter[0]
        empty_rows_before: int = len(
            [row_iter for row_iter in empty_rows if row_iter < galaxy_row]
        )
        galaxy_row += empty_rows_before * expansion

        # Adjust columns coordinates, axis=1
        galaxy_column: int = galaxy_iter[1]
        empty_columns_before: int = len(
            [
                column_iter
                for column_iter in empty_columns
                if column_iter < galaxy_column
            ]
        )
        galaxy_column += empty_columns_before * expansion

        galaxies_adjusted.append((galaxy_row, galaxy_column))

    return galaxies_adjusted


###############################################################################


def measure_distances(galaxies: list[tuple[int, int]]) -> int:
    """
    Takes a list of galaxy coordinates and returns the sum of the lengths of
    the shortest path between each pair of galaxies.

    Parameters
    ----------
    galaxies: list[tuple[int, int]]
        List of galaxy coordinates.

    Returns
    -------
    int
        The sum of all distances.

    Raises
    ------
    None
    """
    distance_sum: int = 0
    for galaxy_iter_a, galaxy_iter_b in itertools.combinations(galaxies, 2):
        # Manhatten metric ||r_2 - r_1|| = |x_2 - x_1| + |y_2 - y_1|
        distance: int = abs(galaxy_iter_b[0] - galaxy_iter_a[0]) + abs(
            galaxy_iter_b[1] - galaxy_iter_a[1]
        )

        distance_sum += distance

    return distance_sum


###############################################################################


def main() -> None:
    """
    Runs the whole script.
    """
    # Performance
    time_start: float = time.perf_counter()

    # Parsing of file
    input_data: np.ndarray = read_file_as_array(os.getcwd() + "/input_day_11")

    # 11-A: Find the sum of path lengths after expansion by 1 line each
    galaxies: list[tuple] = find_galaxies(input_data)

    empty_rows: list[int] = []
    empty_columns: list[int] = []
    empty_rows, empty_columns = find_empty_lines(input_data)

    galaxies_expanded: list[tuple[int, int]] = expand_universe(
        galaxies, empty_rows, empty_columns, 1
    )

    distance_sum: int = measure_distances(galaxies_expanded)
    print(f"Sum of all distances for expandion=1: {distance_sum}")

    # 11-B: Find the sum of path lengths after expansion by factor(!) 1e6, i.e.
    # add 999999 to a total of 1e6 each line.
    galaxies_expanded_more: list[tuple[int, int]] = expand_universe(
        galaxies, empty_rows, empty_columns, 999999
    )

    distance_sum_more: int = measure_distances(galaxies_expanded_more)
    print(f"Sum of all distances for expansion=1e6: {distance_sum_more}")

    # Performance
    time_end: float = time.perf_counter() - time_start
    print(f"{time_end=:.3f}")


main()
