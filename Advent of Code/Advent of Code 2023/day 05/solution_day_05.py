"""
Advent of code 2023, day 05
"""
import os
import time

###############################################################################


def read_file_linewise(filepath: str, head: int = 0):
    """
    Reads the given file (with absolute path) linewise and returns a list of
    all lines

    Parameters
    ----------
    filepath: str
        The absolute path to the target file.
    head: int, default = 0
        The number of lines to read from the beginning of the file.

    Returns
    -------
    list[str]
        The lines of the file as a list.

    Raises
    ------
    None
    """
    input_data: list[str] = []
    with open(filepath, "r", encoding="utf-8") as input_file:
        line_count: int = 0
        for line in input_file:
            input_data.append(line.rstrip())
            line_count += 1
            if line_count == head:
                break

    return input_data


###############################################################################


def parse_input_into_blocks(input_data: list[str]) -> list[list[str]]:
    """
    Takes a list of lines from a text file and finds blocks in the file,
    seperated by empty lines. Each block is written into its own list and a
    list of these lists is returned.

    Parameters
    ----------
    input_data: list[str]
        The input file read linewise as a list.

    Returns
    -------
    list[list[str]]
        A list of each mapping block in the input file as its own list.

    Raises
    ------
    None
    """
    block_list: list[list[str]] = []
    current_block: list[str] = []
    for line_iter in input_data:
        if line_iter != "":
            current_block.append(line_iter)

        if line_iter == "":
            block_list.append(current_block)
            current_block = []
    block_list.append(current_block)

    return block_list


###############################################################################


def parse_blocks(
    block_list: list[list[str]],
) -> list[list]:
    """
    Takes the parsed input data as a list of lists of strings and turns each
    block into a list of ints (seeds) or a list of [int, int, int] of mapping
    instructions. Then returns all those blocks.

    Parameters
    ----------
    block_list: list[list[str]]
        A list of each mapping block in the input file as its own list.

    Returns
    -------
    tuple[list[int] | list[tuple[

    Raises
    ------
    """

    def parse_map_block(map_list: list[str]) -> list[list[int]]:
        # Skip headline
        map_list_data: list[str] = map_list[1:]

        map_list_parsed: list[list[int]] = []
        # Each line consists of three numbers: destination range start, source
        # range start, range length. The resulting tuple will have these three
        # elements in this order.
        for map_iter in map_list_data:
            map_split: list[str] = map_iter.split(" ")
            mapping: list[int] = [
                int(map_instruction) for map_instruction in map_split
            ]
            map_list_parsed.append(mapping)
        return map_list_parsed

    ###########################################################################

    seeds: list[int] = []

    # Seeds need special treatment, as they are only a list of ints, not a list
    # of triples of ints
    string_as_list: list[str] = block_list[0][0].split(" ")
    seeds: list[int] = [int(seed_iter) for seed_iter in string_as_list[1:]]

    return [
        seeds,
        parse_map_block(block_list[1]),
        parse_map_block(block_list[2]),
        parse_map_block(block_list[3]),
        parse_map_block(block_list[4]),
        parse_map_block(block_list[5]),
        parse_map_block(block_list[6]),
        parse_map_block(block_list[7]),
    ]


###############################################################################


def map_this_to_that(source_number: int, mapping: list[list[int]]) -> int:
    """
    Takes a number and uses the mapping to find the number to which the input
    number is mapped by the mapping.

    Parameters
    ----------
    source_number: int
        Takes a number and follows the mapping to the number's target.
    mapping: list[list[int]]
        How each item is mapped to the next.

    Returns
    -------
    int
        The target item number to which the source number is mapped.

    Raises
    ------
    None
    """
    for map_iter in mapping:
        dest_start: int = -1
        source_start: int = -1
        range_length: int = -1
        dest_start, source_start, range_length = map_iter

        if (
            source_number < source_start
            or source_number > source_start + range_length
        ):
            continue

        dest_number: int = dest_start + (source_number - source_start)
        return dest_number

    return source_number


###############################################################################


def map_this_to_that_with_leeway(
    source_number: int, mapping: list[list[int]]
) -> tuple[int, int]:
    """
    Takes a number and uses the mapping to find the number to which the input
    number is mapped by the mapping. Also gives the leeway towards the next
    change in mapping up and down from the source_number.

    Parameters
    ----------
    source_number: int
        Takes a number and follows the mapping to the number's target.
    mapping: list[list[int]]
        How each item is mapped to the next.

    Returns
    -------
    int
        The target item number to which the source number is mapped. Can happen
        to be the unaltered source number.
    int
        The length of the range, that is still left after applying the mappings
        range boundaries.

    Raises
    ------
    None
    """
    dest_number: int = -1
    leeway_up: int = -1
    min_distance_to_next: int = int(1e15)
    for map_iter in mapping:
        dest_start: int = -1
        source_start: int = -1
        range_length: int = -1
        dest_start, source_start, range_length = map_iter

        if source_number < source_start:
            distance_to_next: int = source_start - source_number
            min_distance_to_next = min(distance_to_next, min_distance_to_next)
            continue

        if source_start <= source_number <= source_start + range_length:
            dest_number = dest_start + (source_number - source_start)
            leeway_up = range_length - (dest_number - dest_start)
            return dest_number, leeway_up

    leeway_up = min_distance_to_next
    return source_number, leeway_up


###############################################################################


def find_lowest_used_location(input_data: list[str]) -> int:
    """
    Takes all seed and mapping information and calculates the lowest used plot
    of land, on which a seed is to be planted.

    Parameters
    ----------
    input_data: list[str]
        The input file as a list of lines.

    Returns
    -------
    int
        Location number of the lowest used location.

    Raises
    ------
    None
    """
    # Seperate input into blocks of item types.
    input_blocks: list[list[str]] = parse_input_into_blocks(input_data)

    # Get all mapping blocks
    parsed_blocks: list[list[int] | list[list[int]]] = parse_blocks(
        input_blocks
    )
    seeds: list[int] = parsed_blocks[0]
    map_seed_to_soil: list[list[int]] = parsed_blocks[1]
    map_soil_to_fertilizer: list[list[int]] = parsed_blocks[2]
    map_fertilizer_to_water: list[list[int]] = parsed_blocks[3]
    map_water_to_light: list[list[int]] = parsed_blocks[4]
    map_light_to_temperature: list[list[int]] = parsed_blocks[5]
    map_temperature_to_humidity: list[list[int]] = parsed_blocks[6]
    map_humidity_to_location: list[list[int]] = parsed_blocks[7]

    # Apply mapping blocks to get the lowest number of used location.
    min_location: int = pow(10, 20)
    for seed_iter in seeds:
        soil: int = map_this_to_that(seed_iter, map_seed_to_soil)
        fertilizer: int = map_this_to_that(soil, map_soil_to_fertilizer)
        water: int = map_this_to_that(fertilizer, map_fertilizer_to_water)
        light: int = map_this_to_that(water, map_water_to_light)
        temperatur: int = map_this_to_that(light, map_light_to_temperature)
        humidity: int = map_this_to_that(
            temperatur, map_temperature_to_humidity
        )
        location: int = map_this_to_that(humidity, map_humidity_to_location)

        if location < min_location:
            min_location = location

    return min_location


###############################################################################


def parse_seed_block(seed_block: list[str]) -> list[tuple[int, int]]:
    """
    Takes a string of tuples of seed range start and seed range length and
    turns it into a list of tuples seed range start and seed range end.

    Parameters
    ----------
    seed_block: str
        The block from the input file containing the used seed numbers.

    Returns
    -------
    list[tuple[int, int]]
        The list of seeds being used, each denoting the start and end numbers.

    Raises
    ------
    None
    """
    # Split into headline and each number
    string_as_list: list[str] = seed_block[0].split(" ")
    # Turn all numbers into list
    seeds_all: list[int] = [int(seed_iter) for seed_iter in string_as_list[1:]]

    # Numbers come in pais: seed range start and seed range length
    seed_ranges_starts: list[int] = seeds_all[0::2]
    seed_ranges_lengths: list[int] = seeds_all[1::2]

    # Translate seed range start and length into range start and range end
    seed_ranges_parsed: list[tuple[int, int]] = [
        (start_iter, start_iter + range_iter - 1)
        for start_iter, range_iter in zip(
            seed_ranges_starts, seed_ranges_lengths
        )
    ]

    return seed_ranges_parsed


###############################################################################


def fix_seed_ranges(
    seed_ranges: list[tuple[int, int]], seed_start: int, leeway_up: int
) -> list[tuple[int, int]]:
    """
    Takes a list of intervals with upper and lower boundary and an interval of
    seed start and upper boundary which is to be removed from the first list.

    Parameters
    ----------
    seed_ranges: list[tuple[int, int]]
        The block from the input file containing the used seed numbers.
    seed_start: int
        The start of the range to be removed from the seed range list.
    leeway_up: int
        The length of the range to be removed from the seed range list.

    Returns
    -------
    list[tuple[int, int]]
        List of seed ranges, each with range start and range end.

    Raises
    ------
    None
    """
    new_seed_ranges: list[tuple[int, int]] = []
    for seed_range_start, seed_range_end in seed_ranges:
        if seed_range_start < seed_start < seed_range_end:
            new_seed_ranges.append((seed_range_start, seed_start - 1))
            new_seed_ranges.append(
                (seed_start + leeway_up + 1, seed_range_end)
            )
        elif (
            seed_range_start == seed_start
            and leeway_up < seed_range_end - seed_start
        ):
            new_seed_ranges.append(
                (seed_start + leeway_up + 1, seed_range_end)
            )
        elif (
            seed_range_start == seed_start
            and leeway_up >= seed_range_end - seed_start
        ):
            continue
        else:
            new_seed_ranges.append((seed_range_start, seed_range_end))

    return new_seed_ranges


###############################################################################


def find_lowest_used_location_reinterpreted(input_data: list[str]) -> int:
    """
    Takes all seed and mapping information and calculates the lowest used plot
    of land, on which a seed is to be planted, after fixing the seed numbers.

    Parameters
    ----------
    input_data: list[str]
        The input file as a list of lines.

    Returns
    -------
    int
        Location number of the lowest used location.

    Raises
    ------
    None
    """
    # Seperate input into blocks of item types.
    input_blocks: list[list[str]] = parse_input_into_blocks(input_data)

    # Get all mapping blocks
    parsed_blocks: list[list[int] | list[list[int]]] = parse_blocks(
        input_blocks
    )
    map_seed_to_soil: list[list[int]] = parsed_blocks[1]
    map_soil_to_fertilizer: list[list[int]] = parsed_blocks[2]
    map_fertilizer_to_water: list[list[int]] = parsed_blocks[3]
    map_water_to_light: list[list[int]] = parsed_blocks[4]
    map_light_to_temperature: list[list[int]] = parsed_blocks[5]
    map_temperature_to_humidity: list[list[int]] = parsed_blocks[6]
    map_humidity_to_location: list[list[int]] = parsed_blocks[7]

    # Apply mapping blocks to get the lowest number of used location.
    seed_ranges: list[tuple[int, int]] = parse_seed_block(input_blocks[0])

    # Search for lowest used location number.by reducing the ranges of possible
    # numbers with each seed range passed through the mapping.
    min_location: int = int(1e20)
    seed_previous: int = -1

    while len(seed_ranges) > 0:
        seed: int = seed_ranges[0][0]

        if seed == seed_previous:
            seed_ranges = fix_seed_ranges(seed_ranges, seed, 1)
            continue

        leeway_up: int = -1
        leeway_up_max: int = -1

        soil: int = -1
        soil, leeway_up_max = map_this_to_that_with_leeway(
            seed, map_seed_to_soil
        )

        fertilizer: int = -1
        fertilizer, leeway_up = map_this_to_that_with_leeway(
            soil, map_soil_to_fertilizer
        )
        leeway_up_max = (
            leeway_up
            if leeway_up and leeway_up < leeway_up_max
            else leeway_up_max
        )

        water: int = -1
        water, leeway_up = map_this_to_that_with_leeway(
            fertilizer, map_fertilizer_to_water
        )
        leeway_up_max = (
            leeway_up
            if leeway_up and leeway_up < leeway_up_max
            else leeway_up_max
        )

        light: int = -1
        light, leeway_up = map_this_to_that_with_leeway(
            water, map_water_to_light
        )
        leeway_up_max = (
            leeway_up
            if leeway_up and leeway_up < leeway_up_max
            else leeway_up_max
        )

        temperatur: int = -1
        temperatur, leeway_up = map_this_to_that_with_leeway(
            light, map_light_to_temperature
        )
        leeway_up_max = (
            leeway_up
            if leeway_up and leeway_up < leeway_up_max
            else leeway_up_max
        )

        humidity: int = -1
        humidity, leeway_up = map_this_to_that_with_leeway(
            temperatur, map_temperature_to_humidity
        )
        leeway_up_max = (
            leeway_up
            if leeway_up and leeway_up < leeway_up_max
            else leeway_up_max
        )

        location: int = -1
        location, leeway_up = map_this_to_that_with_leeway(
            humidity, map_humidity_to_location
        )
        leeway_up_max = (
            leeway_up
            if leeway_up and leeway_up < leeway_up_max
            else leeway_up_max
        )

        if location < min_location:
            min_location = location

        seed_ranges = fix_seed_ranges(seed_ranges, seed, leeway_up_max)
        seed_previous = seed

    return min_location - 1


###############################################################################


def main() -> None:
    """
    Runs the whole script.
    """
    # Performance
    time_start: float = time.perf_counter()

    # Parsing of file
    input_data: list[str] = read_file_linewise(os.getcwd() + "/input_day_05")

    # 01-A Find the lowest number of a location, where a seed is planted.
    print(
        "Number of lowest used location: "
        + f"{find_lowest_used_location(input_data)}"
    )

    # 01-B Same as A, but with seed numbers reinterpreted.
    print(
        "Number of lowest used location for reinterpreted seed numbers: "
        + f"{find_lowest_used_location_reinterpreted(input_data)}"
    )

    # Performance
    time_end: float = time.perf_counter() - time_start
    print(f"{time_end=:.3f}")


main()
