"""
Advent of code 2023, day 01
"""
import os
import re
import time


def read_file_linewise(filepath: str, head: int = 0) -> list[str]:
    """
    Reads the given file (with absolute path) linewise and returns a list of
    all lines

    Parameters
    ----------
    filepath: str
        The absolute path to the target file.
    head: int, default = 0
        The number of lines to read from the beginning of the file.

    Returns
    -------
    list[str]
        The lines of the file as a list.

    Raises
    ------
    None
    """
    input_data: list[str] = []
    with open(filepath, "r", encoding="utf-8") as input_file:
        line_count: int = 0
        for line in input_file:
            input_data.append(line.rstrip())
            line_count += 1
            if line_count == head:
                break

    return input_data


###############################################################################


def sum_lines(input_data: list[str]) -> int:
    """
    Takes the lines of the input file, extracts the numbers of each line, then
    sums them up.

    Parameters
    ----------
    input_data: list[str]
        The input file, read linewise.

    Returns
    -------
    int
        The total sum of all line's numbers.

    Raises
    ------
    None
    """
    numbers: list[int] = []
    for line in input_data:
        no_letter_line: str = re.sub(r"[a-zA-Z]", "", line)
        if len(no_letter_line) == 0:
            continue
        if len(no_letter_line) == 1:
            number: int = int(no_letter_line[0] + no_letter_line[0])
            numbers.append(number)
            continue
        if len(no_letter_line) == 2:
            numbers.append(int(no_letter_line))
            continue

        number: int = int(no_letter_line[0] + no_letter_line[-1])
        numbers.append(number)

    return sum(numbers)


###############################################################################


def replace_words_with_numbers(line: str) -> str:
    """
    Takes a string (line of a file), searches for numbers written as words, and
    replaces them with their digit representation.

    Parameters
    ----------
    line: str
        Input line which might contain numbers written as words.

    Returns
    -------
    str
        The line with all numbers as digits.

    Raises
    ------
    None
    """
    digit_words: list[str] = [
        "one",
        "two",
        "three",
        "four",
        "five",
        "six",
        "seven",
        "eight",
        "nine",
    ]
    search_string: str = r"[0-9]|" + "|".join(digit_words)

    line_modified: str = line
    line_substituted: list[str] = []
    while len(line_modified) > 0:
        match: re.Match[str] | None = re.match(search_string, line_modified)
        if match is not None and match.group() in digit_words:
            match_digit: str = str(digit_words.index(match.group()) + 1)
            line_substituted.append(match_digit)
        elif match is not None:
            line_substituted.append(match.group())

        line_modified: str = line_modified[1:]

    return "".join(line_substituted)


###############################################################################


def sum_lines_with_words(input_data: list[str]) -> int:
    """
    Takes the lines of the input file, replaces spelled out numbers, extracts
    the numbers of each line, then sums them up.

    Parameters
    ----------
    input_data: list[str]
        The input file, read linewise.

    Returns
    -------
    int
        The total sum of all line's numbers.

    Raises
    ------
    None
    """
    numbers: list[int] = []
    for line in input_data:
        line_substituted: str = replace_words_with_numbers(line)

        no_letter_line: str = re.sub(r"[a-zA-Z]", "", line_substituted)

        if len(no_letter_line) == 0:
            continue
        if len(no_letter_line) == 1:
            number: int = int(no_letter_line[0] + no_letter_line[0])
            numbers.append(number)
            continue
        if len(no_letter_line) == 2:
            numbers.append(int(no_letter_line))
            continue

        number: int = int(no_letter_line[0] + no_letter_line[-1])
        numbers.append(number)

    return sum(numbers)


###############################################################################


def main() -> None:
    """
    Runs the whole script.
    """
    # Performance
    time_start: float = time.perf_counter()

    # Parsing of file
    input_data: list[str] = read_file_linewise(os.getcwd() + "/input_day_01")

    # 01-A Sum of extracted numbers:
    print(f"Sum of parsed lines: {sum_lines(input_data)}")

    # 01-B Sum of extracted numbers, words and digits:
    print(
        "Sum of parsed lines, with number words: "
        + f"{sum_lines_with_words(input_data)}"
    )

    # Performance
    time_end: float = time.perf_counter() - time_start
    print(f"{time_end=:.3f}")


main()
