"""
Advent of code 2023, day 09
"""
import os
import time


def read_file_linewise(filepath: str, head: int = 0) -> list[str]:
    """
    Reads the given file (with absolute path) linewise and returns a list of
    all lines

    Parameters
    ----------
    filepath: str
        The absolute path to the target file.
    head: int, default = 0
        The number of lines to read from the beginning of the file.

    Returns
    -------
    list[str]
        The lines of the file as a list.

    Raises
    ------
    None
    """
    input_data: list[str] = []
    with open(filepath, "r", encoding="utf-8") as input_file:
        line_count: int = 0
        for line in input_file:
            input_data.append(line.rstrip())
            line_count += 1
            if line_count == head:
                break

    return input_data


###############################################################################


def parse_dataset(input_data: list[str]) -> list[list[int]]:
    """
    Takes a list of lines with numbers seperated by spaces and returns a list
    of list with each line and each number of each line seperated and converted
    to int.

    Parameters
    ----------
    input_data: list[list[str]]
        The input data.

    Returns
    -------
    listlist[[int]]
        The data seperated into int.

    Raises
    ------
    None
    """
    parsed_input: list[list[int]] = []
    for line in input_data:
        line_split: list[str] = line.split(" ")
        line_as_int: list[int] = [int(item_iter) for item_iter in line_split]
        parsed_input.append(line_as_int)
    return parsed_input


###############################################################################


def process_line(line: list[int], past: bool = False) -> int:
    """
    Takes a line as a list of int and returns the prediction for the next
    value.

    Parameters
    ----------
    line: list[int]
        A list of int values for which to predict the next.
    past: bool, default: False
        If False, extrapolates to future, if True, extrapolates to past.

    Returns
    -------
    int
        The next value prediction.

    Raises
    ------
    None
    """
    if past:
        direction = 0
        operation = -1
    else:
        direction = -1
        operation = +1

    # Calculate derivatives
    derivatives: list[list[int]] = [line]
    derivative_this: list[int] = line
    while any(derivative_this):
        derivative_new: list[int] = []
        for counter in range(0, len(derivative_this) - 1):
            derivative_new.append(
                derivative_this[counter + 1] - derivative_this[counter]
            )
        derivatives.append(derivative_new)
        derivative_this = derivative_new

    # Calculate prediction
    prediction: int = 0
    for counter in range(len(derivatives) - 1, -1, -1):
        prediction = derivatives[counter][direction] + operation * prediction

    return prediction


###############################################################################


def sum_all_predictions(input_data: list[str], past: bool = False) -> int:
    """
    Takes the input file read linewise, extrapolates the next values, and sums
    them up.

    Parameters
    ----------
    input_data: list[str]
        The input file, read linewise.
    past: bool, default: False
        If False, extrapolates to future, if True, extrapolates to past.

    Returns
    -------
    int
        The sum of all prediction values.

    Raises
    ------
    None
    """
    input_parsed: list[list[int]] = parse_dataset(input_data)

    sum_of_results: int = 0
    for line in input_parsed:
        sum_of_results += process_line(line, past)

    return sum_of_results


###############################################################################


def main() -> None:
    """
    Runs the whole script.
    """
    # Performance
    time_start: float = time.perf_counter()

    # Parsing of file
    input_data: list[str] = read_file_linewise(os.getcwd() + "/input_day_09")

    # 09-A Sum of future predictions:
    print(
        "Sum of all future predictions: "
        + f"{sum_all_predictions(input_data)}"
    )

    # 09-B Sum of past predictions:
    print(
        "Sum of all past predictions: "
        + f"{sum_all_predictions(input_data, True)}"
    )

    # Performance
    time_end: float = time.perf_counter() - time_start
    print(f"{time_end=:.3f}")


main()
