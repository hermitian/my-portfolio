"""
Advent of code 2023, day 04.
"""
import os
import re
import time
from typing import TypedDict

###############################################################################


class Game(TypedDict):
    """
    Dict with correctly typed fields.
    """

    game_number: int
    numbers_played: list[int]
    numbers_winning: list[int]
    points: int


###############################################################################


def read_file_linewise(filepath: str, head: int = 0):
    """
    Reads the given file (with absolute path) linewise and returns a list of
    all lines

    Parameters
    ----------
    filepath: str
        The absolute path to the target file.
    head: int, default = 0
        The number of lines to read from the beginning of the file.

    Returns
    -------
    list[str]
        The lines of the file as a list.

    Raises
    ------
    None
    """
    input_data: list[str] = []
    with open(filepath, "r", encoding="utf-8") as input_file:
        line_count: int = 0
        for line in input_file:
            input_data.append(line.rstrip())
            line_count += 1
            if line_count == head:
                break

    return input_data


###############################################################################


def parse_line(line: str) -> Game:
    """
    Takes a line of strings and extracts the game number, the played numbers,
    and the winning numbers, and returns them as a dict.

    Parameters
    ----------
    line: str
        A single line with all information about a single game.

    Returns
    -------
    Game
        A dict with all information about a single game, neatly packaged.

    Raises
    ------
    None
    """
    # Split game meta data and numbers
    numbers_complete: str = ""
    game_name_complete, numbers_complete = line.split(":")

    game_name: re.Match[str] | None = re.search(
        r"[0-9]{1,3}", game_name_complete
    )

    game_number: int = -1
    if game_name is not None:
        game_number = int(game_name.group())

    # Split card numbers and winning numbers
    numbers_played_str: str = ""
    numbers_winning_str: str = ""
    numbers_played_str, numbers_winning_str = numbers_complete.split("|")

    numbers_played: list[int] = [
        int(number) for number in re.findall(r"[0-9]{1,2}", numbers_played_str)
    ]
    numbers_winning: list[int] = [
        int(number)
        for number in re.findall(r"[0-9]{1,2}", numbers_winning_str)
    ]

    # Collect game information into dict.
    game_dict: Game = {
        "game_number": game_number,
        "numbers_played": numbers_played,
        "numbers_winning": numbers_winning,
        "points": 0,
    }

    # Add points
    game_dict["points"] = get_game_points(game_dict)

    return game_dict


###############################################################################


def get_game_points(game_dict: Game) -> int:
    """
    Takes a game dict and calculates the winning numbers and the total score of
    the game.

    Parameters
    ----------
    game_dict: Game
        The dict with all information about a single game.

    Returns
    -------
    int
        The total points for the given game.

    Raises
    ------
    None
    """
    numbers_played: list[int] = game_dict["numbers_played"]
    numbers_winning: list[int] = game_dict["numbers_winning"]

    hits: int = 0
    for number in numbers_played:
        if number in numbers_winning:
            hits += 1

    points_total: int = 0 if hits == 0 else pow(2, hits - 1)

    return points_total


###############################################################################


def sum_game_points(input_data: list[str]) -> int:
    """
    Takes a list of game strings, calculates each game's points and the sum.

    Parameters
    ----------
    input_data: list[str]
        A list of strings with all information about a single game each.

    Returns
    -------
    int
        Sum of all game points.

    Raises
    ------
    None
    """
    points_total: int = 0
    for line_iter in input_data:
        game_dict: Game = parse_line(line_iter)
        points_total += game_dict["points"]

    return points_total


###############################################################################


def get_game_hits(game_dict: Game) -> int:
    """
    Takes a game dict and calculates the winning numbers and the total number
    of hit winning numbers in this game.

    Parameters
    ----------
    game_dict: Game
        A dict with all information about a single game.

    Returns
    -------
    int
        How many winning numbers were hit.

    Raises
    ------
    None
    """
    numbers_played: list[int] = game_dict["numbers_played"]
    numbers_winning: list[int] = game_dict["numbers_winning"]

    hits: int = 0
    for number in numbers_played:
        if number in numbers_winning:
            hits += 1

    return hits


###############################################################################


def get_number_of_cards(input_data: list[str]) -> int:
    """
    Takes a list of game strings, calculates all free card wins, and outputs
    the total number of cards.

    Parameters
    ----------
    input_data: list[str]
        A list of strings with all information about a single game each.

    Returns
    -------
    int
        Number of cards.

    Raises
    ------
    None
    """

    game_count: int = len(input_data)

    # Get all games' number of winning number hits.
    hits_in_games: list[int] = [0]
    for line_iter in input_data:
        game_this: Game = parse_line(line_iter)
        hits_in_games.append(get_game_hits(game_this))

    # Add cards for each hit in a game.
    card_counts: list[int] = [0]
    card_counts.extend([1 for _ in range(len(hits_in_games) - 1)])

    # Deal with recursively won extra cards.
    for game_iter, _ in enumerate(card_counts):
        numbers_to_max: int = game_count - game_iter
        for forward_counter in range(
            1 if numbers_to_max > 0 else 0,
            min(hits_in_games[game_iter] + 1, numbers_to_max + 1)
            if numbers_to_max > 0
            else 0,
        ):
            card_counts[game_iter + forward_counter] += card_counts[game_iter]

    return sum(card_counts)


###############################################################################


def main() -> None:
    """
    Runs the whole script.
    """
    # Performance
    time_start: float = time.perf_counter()

    # Parsing of file
    input_data: list[str] = read_file_linewise(os.getcwd() + "/input_day_04")

    # 01-A Sum of winning cards' points:
    print(f"Sum of points: {sum_game_points(input_data)}")

    # 01-B Get total amount of cards, if cards can win extra cards:
    print(f"Total number of scratchcards: {get_number_of_cards(input_data)}")

    # Performance
    time_end: float = time.perf_counter() - time_start
    print(f"{time_end=:.3f}")


main()
