"""
Advent of code 2023, day 16
"""
import os
import sys
import time

import numpy as np

###############################################################################


def read_file_as_array(filepath: str) -> np.ndarray:
    """
    Reads the given file (with absolute path) into a 2d array with each
    character as an element of the inner array.

    Parameters
    ----------
    filepath: str
        The absolute path to the target file.

    Returns
    -------
    np.char.asarray
        The lines of the file as a list of lists, each character being an
        element of the inner lists.

    Raises
    ------
    None
    """
    input_data: list[list[str]] = []
    with open(filepath, "r", encoding="utf-8") as input_file:
        for line in input_file:
            line_content: str = line.rstrip()
            line_array: list[str] = list(line_content)
            input_data.append(line_array)

    return np.array(input_data)


###############################################################################


def energize_grid(grid: np.ndarray, any_start: bool = False) -> int:
    """
    Takes a 2d array with mirrors and creates a path of light.

    Parameters
    ----------
    grid: np.ndarray
        The original grid.
    any_start: bool, default=False
        If True, checks every possible starting point and returns the grid with
        the maximum number of energized tiles.

    Returns
    -------
    np.ndarray
        The original grid but energized, i.e. with the travel path of the light
        beam drawn in.

    Raises
    ------
    None
    """
    ###########################################################################
    # Constants
    ###########################################################################

    GRID_SIZE_X: int = grid.shape[1]
    GRID_SIZE_Y: int = grid.shape[0]

    ###########################################################################
    # Functions/Methods
    ###########################################################################

    def create_all_starting_points() -> list[tuple[int, int, int]]:
        """
        Creates all possible starting points:
            All left borders, moving to the right,
            all right borders, moving to the left,
            all top borders, moving down,
            all bottom borders, moving up.
        """
        starting_points: list[tuple[int, int, int]] = []

        # Left to right
        for row_iter in range(GRID_SIZE_X):
            starting_points.append((row_iter, 0, 1))
        # Right to left
        for row_iter in range(GRID_SIZE_X):
            starting_points.append((row_iter, GRID_SIZE_Y - 1, 3))
        for column_iter in range(GRID_SIZE_Y):
            starting_points.append((0, column_iter, 2))
        for column_iter in range(GRID_SIZE_Y):
            starting_points.append((GRID_SIZE_X - 1, column_iter, 4))

        return starting_points

    ###########################################################################

    def move_next_step(position: tuple[int, int], direction: int) -> None:
        """
        Moves from a given position to the next one, following mirrors.
        Recursively calls itself, twice if a splitting mirror is hit.
        Returns, when the next step would lead out of bounds or on an energized
        field with the same travel direction.

        Directions: 1: E, 2: S, 3: W, 4: N
        """

        # Check if current field oob -> return
        if (
            position[0] < 0
            or position[0] >= GRID_SIZE_Y
            or position[1] < 0
            or position[1] >= GRID_SIZE_X
        ):
            return

        # Check if current field already walked in same direction -> return
        path_already_walked: bool = (
            str(direction) in grid_traveled[position[0], position[1]]
        )
        if path_already_walked:
            return

        # Energize current field with direction info
        grid_traveled[position[0], position[1]] += str(direction)

        # Check if current field mirror
        # -> recursively call for changed direction and new coordinates.
        # -> recursively call twice for splitting mirror, changed direction and
        #   new coordinates
        # return
        if grid[position[0], position[1]] == "/":
            if direction == 1:
                move_next_step((position[0] - 1, position[1]), 4)
            elif direction == 2:
                move_next_step((position[0], position[1] - 1), 3)
            elif direction == 3:
                move_next_step((position[0] + 1, position[1]), 2)
            else:
                move_next_step((position[0], position[1] + 1), 1)
        elif grid[position[0], position[1]] == "\\":
            if direction == 1:
                move_next_step((position[0] + 1, position[1]), 2)
            elif direction == 2:
                move_next_step((position[0], position[1] + 1), 1)
            elif direction == 3:
                move_next_step((position[0] - 1, position[1]), 4)
            else:
                move_next_step((position[0], position[1] - 1), 3)
        elif grid[position[0], position[1]] == "-":
            if direction == 1:
                move_next_step((position[0], position[1] + 1), 1)
            elif direction == 2:
                move_next_step((position[0], position[1] - 1), 3)
                move_next_step((position[0], position[1] + 1), 1)
            elif direction == 3:
                move_next_step((position[0], position[1] - 1), 3)
            else:
                move_next_step((position[0], position[1] - 1), 3)
                move_next_step((position[0], position[1] + 1), 1)
        elif grid[position[0], position[1]] == "|":
            if direction == 1:
                move_next_step((position[0] - 1, position[1]), 4)
                move_next_step((position[0] + 1, position[1]), 2)
            elif direction == 2:
                move_next_step((position[0] + 1, position[1]), 2)
            elif direction == 3:
                move_next_step((position[0] - 1, position[1]), 4)
                move_next_step((position[0] + 1, position[1]), 2)
            else:
                move_next_step((position[0] - 1, position[1]), 4)
        else:
            if direction == 1:
                move_next_step((position[0], position[1] + 1), 1)
            elif direction == 2:
                move_next_step((position[0] + 1, position[1]), 2)
            elif direction == 3:
                move_next_step((position[0], position[1] - 1), 3)
            else:
                move_next_step((position[0] - 1, position[1]), 4)

        return

    ###########################################################################
    # Execution
    ###########################################################################

    # Move light through grid, starting in the top left coming from the left, or
    # go through all starting points, if any_start is True.
    starting_points: list[tuple[int, int, int]] = []
    if not any_start:
        starting_points = [(0, 0, 0)]
    else:
        starting_points = create_all_starting_points()

    max_energized_tiles: int = 0
    for start_x, start_y, start_direction in starting_points:
        # Create an empty copy of grid
        grid_traveled: np.ndarray = np.zeros_like(grid, dtype="str")

        # Energize
        move_next_step((start_x, start_y), start_direction)

        # Count energized fields
        unique, counts = np.unique(grid_traveled, return_counts=True)
        uniques: dict[str, int] = dict(zip(unique, counts))
        energized_tile_count: int = GRID_SIZE_X * GRID_SIZE_Y - uniques[""]
        if energized_tile_count > max_energized_tiles:
            max_energized_tiles = energized_tile_count

        # print(
        #     f"starting_point: ({start_x}, {start_y}), "
        #     + f"starting_direction: {start_direction}, "
        #     + f"energized_tiles: {grid.shape[0] * grid.shape[1] - uniques['']}"
        # )

    return max_energized_tiles


###############################################################################


def get_energized_tile_count(
    input_data: np.ndarray, any_start: bool = False
) -> int:
    """
    Takes a 2d array of mirrors, applies the light beam, and counts the
    energized tiles.

    Parameters
    ----------
    input_data: np.ndarray
        The 2d array.
    any_start: bool, default=False
        If True, checks every possible starting point and returns the grid with
        the maximum number of energized tiles.

    Returns
    -------
    int
        The number of energized tiles.

    Raises
    ------
    None
    """
    energized_tile_count: int = energize_grid(input_data, any_start)

    return energized_tile_count


###############################################################################


def main() -> None:
    """
    Runs the whole script.
    """
    # Performance
    time_start: float = time.perf_counter()

    # Parsing of file
    input_data: np.ndarray = read_file_as_array(os.getcwd() + "/input_day_16")

    sys.setrecursionlimit(5000)
    # 15-A: Find number of travelled tiles:
    print(f"Number of energized tiles: {get_energized_tile_count(input_data)}")

    # 15-B: Find number of travelled tiles, any starting point:
    print(
        "Number of energized tiles, any starting point: "
        + f"{get_energized_tile_count(input_data, True)}"
    )

    # Performance
    time_end: float = time.perf_counter() - time_start
    print(f"{time_end=:.3f}")


main()
