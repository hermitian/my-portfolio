"""
Advent of code 2023, day 07
"""
import os
import time
from typing import TypedDict

import numpy as np
import pandas as pd

###############################################################################

CARD_VALUE: dict[str, int] = {
    "2": 2,
    "3": 3,
    "4": 4,
    "5": 5,
    "6": 6,
    "7": 7,
    "8": 8,
    "9": 9,
    "T": 10,
    "J": 11,
    "Q": 12,
    "K": 13,
    "A": 14,
}

CARD_VALUE_JOKER: dict[str, int] = {
    "2": 2,
    "3": 3,
    "4": 4,
    "5": 5,
    "6": 6,
    "7": 7,
    "8": 8,
    "9": 9,
    "T": 10,
    "J": 1,
    "Q": 12,
    "K": 13,
    "A": 14,
}

CATEGORY_MAP_FOR_JOKER: list[int] = [
    0,
    2,
    4,
    5,
    6,
    7,
    7,
    7,
]

###############################################################################


class Hand(TypedDict):
    hand: str
    bid: int
    power: int
    category: int
    first: int
    second: int
    third: int
    fourth: int
    fifth: int


###############################################################################


def read_file_linewise(filepath: str, head: int = 0) -> list[str]:
    """
    Reads the given file (with absolute path) linewise and returns a list of
    all lines

    Parameters
    ----------
    filepath: str
        The absolute path to the target file.
    head: int, default = 0
        The number of lines to read from the beginning of the file.

    Returns
    -------
    list[str]
        The lines of the file as a list.

    Raises
    ------
    None
    """
    input_data: list[str] = []
    with open(filepath, "r", encoding="utf-8") as input_file:
        line_count: int = 0
        for line in input_file:
            input_data.append(line.rstrip())
            line_count += 1
            if line_count == head:
                break

    return input_data


###############################################################################


def parse_line(input_data: str) -> Hand:
    """
    Takes a line from a text file and seperates it into the two segments in the
    line, the first being a string, the second an int.
    """
    segment_seperated: list[str] = input_data.split(" ")
    segment_list: Hand = {
        "hand": segment_seperated[0],
        "bid": int(segment_seperated[1]),
        "power": 0,
        "category": 0,
        "first": 0,
        "second": 0,
        "third": 0,
        "fourth": 0,
        "fifth": 0,
    }

    return segment_list


###############################################################################


def calculate_hand_power(hand: Hand) -> Hand:
    """
    Takes a list of a string containing the hand and an int with the hands bid
    and returns a list of the hand's string, the bid, the rank in terms of the
    category of the hand (straight, full house, ...), a list element for each
    of the hand's cards in their order with T, J, Q, K, A turned into 10, 11,
    12, 13, 14, respectively.

    Parameters
    ----------
    hand: Hand

    Returns
    -------
    Hand

    Raises
    ------
    None
    """
    hand_complete: Hand = {
        "hand": hand["hand"],
        "bid": hand["bid"],
        "power": hand["power"],
        "category": 0,
        "first": 0,
        "second": 0,
        "third": 0,
        "fourth": 0,
        "fifth": 0,
    }

    # Original hand's cards, each as a list element in its own column, with
    # joker as value 1
    hand_complete["first"] = CARD_VALUE[hand["hand"][0]]
    hand_complete["second"] = CARD_VALUE[hand["hand"][1]]
    hand_complete["third"] = CARD_VALUE[hand["hand"][2]]
    hand_complete["fourth"] = CARD_VALUE[hand["hand"][3]]
    hand_complete["fifth"] = CARD_VALUE[hand["hand"][4]]

    # Category of hand
    hand_uniques: set = set(hand["hand"])
    hand_unique_count: list[int] = sorted(
        [hand["hand"].count(uniques) for uniques in hand_uniques],
        reverse=True,
    )

    category: int = 0
    # Five of a kind
    if hand_unique_count[0] == 5:
        category = 7
    # Four of a kind
    elif hand_unique_count[0] == 4:
        category = 6
    # Three of a kind vs. Full House
    elif hand_unique_count[0] == 3 and hand_unique_count[1] == 2:
        category = 5
    elif hand_unique_count[0] == 3:
        category = 4
    # Two pair vs. one pair
    elif hand_unique_count[0] == 2 and hand_unique_count[1] == 2:
        category = 3
    elif hand_unique_count[0] == 2:
        category = 2
    # High card
    else:
        category = 1
    hand_complete["category"] = category

    return hand_complete


###############################################################################


def calculate_hand_power_with_joker(hand: Hand) -> Hand:
    """
    Takes a list of a string containing the hand and an int with the hands bid
    and returns a list of the hand's string, the bid, the rank in terms of the
    category of the hand (straight, full house, ...), a list element for each
    of the hand's cards in their order with T, J, Q, K, A turned into 10, 11,
    12, 13, 14, respectively. Here, Jokers count as 1 but are also wildcards.

    Parameters
    ----------
    hand: Hand

    Returns
    -------
    Hand

    Raises
    ------
    None
    """

    hand_complete: Hand = {
        "hand": hand["hand"],
        "bid": hand["bid"],
        "power": hand["power"],
        "category": 0,
        "first": 0,
        "second": 0,
        "third": 0,
        "fourth": 0,
        "fifth": 0,
    }

    # Category after considering jokers
    hand_uniques: set = set(hand_complete["hand"])
    hand_unique_count: list[int] = sorted(
        [hand["hand"].count(uniques) for uniques in hand_uniques],
        reverse=True,
    )

    category: int = 0
    upgrade_to_cat_six_possible: bool = False
    # Five of a kind
    if hand_unique_count[0] == 5:
        category = 7
    # Four of a kind
    elif hand_unique_count[0] == 4:
        category = 6
    # Three of a kind vs. Full House
    elif hand_unique_count[0] == 3 and hand_unique_count[1] == 2:
        category = 5
    elif hand_unique_count[0] == 3:
        category = 4
    # Two pair vs. one pair
    elif hand_unique_count[0] == 2 and hand_unique_count[1] == 2:
        if hand["hand"].count("J") == 2:
            upgrade_to_cat_six_possible = True
        category = 3
    elif hand_unique_count[0] == 2:
        category = 2
    # High card
    else:
        category = 1

    if "J" in hand_complete["hand"]:
        if category == 3 and upgrade_to_cat_six_possible:
            category = 6
        else:
            category = CATEGORY_MAP_FOR_JOKER[category]
    hand_complete["category"] = category

    # Original hand's cards, each as a list element in its own column, with
    # joker as value 1
    hand_complete["first"] = CARD_VALUE_JOKER[hand["hand"][0]]
    hand_complete["second"] = CARD_VALUE_JOKER[hand["hand"][1]]
    hand_complete["third"] = CARD_VALUE_JOKER[hand["hand"][2]]
    hand_complete["fourth"] = CARD_VALUE_JOKER[hand["hand"][3]]
    hand_complete["fifth"] = CARD_VALUE_JOKER[hand["hand"][4]]

    return hand_complete


###############################################################################


def sum_hand_powers(input_data: list[str], joker: bool = False) -> int:
    """
    Takes the input file as a list of lines, parses each line, calculates each
    hand's power and sums all powers up.

    Parameters
    ----------
    input_data: list[str]
        The input file, read linewise.
    joker: bool, default: False
        If False, "J" will be counted as 11 with no additional function. If
        True, "J" will be counted as 1 and can function as a wildcard.

    Returns
    -------
    int
        The total sum of hand powers.

    Raises
    ------
    None
    """

    # Parsing of lines and calculating each hand's power
    hands_all: list[Hand] = []
    for hand_iter in input_data:
        hand_complete: Hand = parse_line(hand_iter)
        if joker:
            hand_complete = calculate_hand_power_with_joker(hand_complete)
        else:
            hand_complete = calculate_hand_power(hand_complete)
        hands_all.append(hand_complete)

    # Ranking as pd.DataFrame
    df_columns = [
        "hand",
        "bid",
        "power",
        "category",
        "first",
        "second",
        "third",
        "fourth",
        "fifth",
    ]
    hands_df: pd.DataFrame = pd.DataFrame(hands_all, columns=df_columns)

    hands_df.sort_values(
        by=[
            "category",
            "first",
            "second",
            "third",
            "fourth",
            "fifth",
        ],
        inplace=True,
        ignore_index=True,
    )

    # Sum up winnings
    sum_of_winnings: int = 0
    for hand_counter, hand_iter in hands_df.iterrows():
        winnings: int = (hand_counter + 1) * int(hand_iter["bid"])
        sum_of_winnings += winnings

    return sum_of_winnings


###############################################################################


def main() -> None:
    """
    Runs the whole script.
    """
    # Performance
    time_start: float = time.perf_counter()

    # Parsing of file
    input_data: list[str] = read_file_linewise(os.getcwd() + "/input_day_07")

    # 07-A Sum of hand powers:
    print(f"Sum of hand powers: {sum_hand_powers(input_data)}")

    # 07-B Sum of hand powers, with Jokers:
    print(
        f"Sum of hand powers, with Jokers: {sum_hand_powers(input_data, True)}"
    )

    # Performance
    time_end: float = time.perf_counter() - time_start
    print(f"{time_end=:.3f}")


main()
