"""
Advent of code 2023, day 02
"""
import os
import re
import time

###############################################################################


RED_MAX: int = 12
GREEN_MAX: int = 13
BLUE_MAX: int = 14


###############################################################################


def read_file_linewise(filepath: str, head: int = 0) -> list[str]:
    """
    Reads the given file (with absolute path) linewise and returns a list of
    all lines

    Parameters
    ----------
    filepath: str
        The absolute path to the target file.
    head: int, default = 0
        The number of lines to read from the beginning of the file.

    Returns
    -------
    list[str]
        The lines of the file as a list.

    Raises
    ------
    None
    """
    input_data: list[str] = []
    with open(filepath, "r", encoding="utf-8") as input_file:
        line_count: int = 0
        for line in input_file:
            input_data.append(line.rstrip())
            line_count += 1
            if line_count == head:
                break

    return input_data


###############################################################################


def parse_draw(single_draw: str) -> dict[str, int]:
    """
    Takes a draw in string form and turns it into a dictionary.

    Parameters
    ----------
    single_draw: str
        The string representing a single draw of cubes.

    Returns
    -------
    dict[str, int]
        A dict of which colors were drawn how many times.

    Raises
    ------
    None
    """
    colors_seperated: list[str] = re.split(r",", single_draw)

    dict_of_colors: dict = {"red": 0, "blue": 0, "green": 0}
    for single_color in colors_seperated:
        color_name: str = re.findall(r"red|green|blue", single_color)[0]
        color_count: int = int(re.findall(r"[0-9]{1,}", single_color)[0])

        dict_of_colors[color_name] = color_count

    return dict_of_colors


###############################################################################


def calculate_game_power(list_of_draws: list[dict[str, int]]) -> int:
    """
    Calculates the product of the minimum number of cubes of each color
    necessary for the outcome of the game to be possible.
    """
    min_red: int = 0
    min_green: int = 0
    min_blue: int = 0

    for single_draw in list_of_draws:
        if single_draw["red"] > min_red:
            min_red = single_draw["red"]
        if single_draw["green"] > min_green:
            min_green = single_draw["green"]
        if single_draw["blue"] > min_blue:
            min_blue = single_draw["blue"]

    return min_red * min_blue * min_green


###############################################################################


def parse_line(line: str) -> dict[str, int | list[dict]]:
    """
    Splits each line to extract the game id and all sets of draws

    Parameters
    ----------
    line: str
        A single line of the input file, containing a complete game.

    Returns
    -------
    dict[str, int | list[str]]
        A dict containing the game ID and each draw as a list entry.

    Raises
    ------
    None
    """
    split_game_id: list[str] = re.split(r":", line)
    game_id: int = int(re.findall(r"[0-9]{1,2}", split_game_id[0])[0])

    list_of_draws: list[str] = re.split(r";", split_game_id[1])

    list_of_draws_parsed: list[dict[str, int]] = []
    for single_draw in list_of_draws:
        list_of_draws_parsed.append(parse_draw(single_draw))

    game_power = calculate_game_power(list_of_draws_parsed)

    game_dict: dict[str, int | list[dict]] = {
        "game_id": game_id,
        "set_of_draws": list_of_draws_parsed,
        "power": game_power,
    }
    return game_dict


###############################################################################


def check_for_valid_game(game_dict: dict[str, int | list[str]]) -> bool:
    """
    Checks if the given set of draws is valid

    Parameters
    ----------
    game_dict: dict
        A set of drawn cubes, representing a complete game.

    Returns
    -------
    bool
        True if the given game is valid.

    Raises
    ------
    None
    """
    for single_draw in game_dict["set_of_draws"]:
        if (
            single_draw["red"] > RED_MAX
            or single_draw["blue"] > BLUE_MAX
            or single_draw["green"] > GREEN_MAX
        ):
            return False

    return True


###############################################################################


def sum_of_game_ids(input_data: list[str]) -> int:
    """
    Takes the input file read linewise and calculates the IDs of possible
    games.

    Parameters
    ----------
    input_data: str
        The input file, read linewise.

    Returns
    -------
    int
        Sum of IDs of possible games.

    Raises
    ------
    None
    """
    valid_game_ids: list[int] = []
    for line in input_data:
        game_dict: dict = parse_line(line)
        if check_for_valid_game(game_dict):
            valid_game_ids.append(game_dict["game_id"])

    return sum(valid_game_ids)


###############################################################################


def sum_game_powers(input_data: list[str]) -> int:
    """
    Takes the input file read linewise, parses it into valid games, and sums up
    the power of each game's necessary cubes.

    Parameters
    ----------
    input_data: str
        The input file, read linewise.

    Returns
    -------
    int
        Sum of all game powers.

    Raises
    ------
    None
    """
    power_sum: int = 0
    for line in input_data:
        game_dict: dict = parse_line(line)
        power_sum += game_dict["power"]

    return power_sum


###############################################################################


def main() -> None:
    """
    Runs the whole script.
    """
    # Performance
    time_start: float = time.perf_counter()

    # Parsing of file
    input_data: list[str] = read_file_linewise(os.getcwd() + "/input_day_02")

    # 02-A: Sum IDs of possible games
    print(f"Sum of possible game IDs: {sum_of_game_ids(input_data)}")

    # 02-B Minimum set of needed cubes:
    print(f"Sum of powers of necessary cubes: {sum_game_powers(input_data)}")

    # Performance
    time_end: float = time.perf_counter() - time_start
    print(f"{time_end=:.3f}")


main()
