"""
Advent of code 2023, day 08
"""
import os
import time

import numpy


def read_file_linewise(filepath: str, head: int = 0) -> list[str]:
    """
    Reads the given file (with absolute path) linewise and returns a list of
    all lines

    Parameters
    ----------
    filepath: str
        The absolute path to the target file.
    head: int, default = 0
        The number of lines to read from the beginning of the file.

    Returns
    -------
    list[str]
        The lines of the file as a list.

    Raises
    ------
    None
    """
    input_data: list[str] = []
    with open(filepath, "r", encoding="utf-8") as input_file:
        line_count: int = 0
        for line in input_file:
            input_data.append(line.rstrip())
            line_count += 1
            if line_count == head:
                break

    return input_data


###############################################################################


def parse_moveset(move_set: str) -> list[int]:
    """
    Takes a moveset written as a string of 'L' and 'R' and turns it into a list
    of 0 and 1, respectively.

    Parameters
    ----------
    move_set: str
        The movement instructions as a single string of 'L' and 'R'.

    Returns
    -------
    list[int]
        The movement instructions as a list of 0 and 1.

    Raises
    ------
    None
    """
    map_str_to_int: dict[str, int] = {"L": 0, "R": 1}
    move_set_list: list[int] = [
        map_str_to_int[move_iter] for move_iter in move_set
    ]
    return move_set_list


###############################################################################


def parse_nodeset(node_set: list[str]) -> dict[str, list[str]]:
    """
    Takes a list of strings (lines from the input file) and turns it into a
    dict with formatting
    {'node_name': ['target_left', 'target_right']}.

    Parameters
    ----------
    node_set: list[str]
        A list of three-letter-nodes with a tuple of target nodes, i.e.
        "AAA = (BBB, CCC)"

    Returns
    -------
    dict[str, any]
        A dict with each node as a key and its target nodes as a list of
        values.

    Raises
    ------
    None
    """
    nodes_parsed: dict[str, list[str]] = {}

    for node_iter in node_set:
        node_name: str = ""
        target_nodes: str = ""
        node_name, target_nodes = node_iter.split(" = ")

        target_nodes = target_nodes.strip("()")
        target_node_left: str = ""
        target_node_right: str = ""
        target_node_left, target_node_right = target_nodes.split(", ")

        nodes_parsed[node_name] = [target_node_left, target_node_right]

    return nodes_parsed


###############################################################################


def walk_to_exit(
    move_set: list[int],
    node_set: dict[str, list[str]],
    first_node: str,
    last_node: str,
) -> int:
    """
    Takes both the prepared move set and node set and walks them from the first
    node until the last node is reached.

    Parameters
    ----------
    move_set: list[int]
        The move set as a list of 0 and 1.
    node_set: dict[str, list[str]]
        The node set as a dict with each node as key and the list of left/right
        target nodes as value.
    first_node: str
        The node where the journey starts.
    last_node: str
        The node where the journey ends.

    Returns
    -------
    int
        The number of steps necessary to get from the first to the last node.

    Raises
    ------
    None
    """
    time_start: float = time.perf_counter()
    step_count: int = 0
    current_node: str = first_node

    while current_node != last_node:
        next_move: int = move_set[step_count % len(move_set)]
        current_node = node_set[current_node][next_move]
        step_count += 1
        if not step_count % 1000000:
            time_now: float = time.perf_counter() - time_start
            print(
                f"\r{current_node=}, {step_count=:g}, {time_now=:.2f} s",
                end="",
            )

    return step_count


###############################################################################


def find_shortest_loop_length(
    move_set: list[int], node_dict: dict[str, list[str]], start_node: str
) -> tuple[int, int]:
    """
    Takes a move set, a node dictionary, and a starting node and finds the
    shortest length to get back to starting node with complete usage of the
    move set.

    Parameters
    ----------
    move_set: list[int]
        List of movement instructions as 0 and 1.
    node_dict: dict[str, list[str]]
        Dictionary of all nodes and their edges.
    start_node: str
        Node where to start.

    Returns
    -------
    int
        The path length to complete the circle back to start node with complete
        usage of the move set.

    Raises
    ------
    None
    """
    number_of_steps: int = 0
    current_node: str = start_node
    visited_nodes: list[str] = []
    while current_node not in visited_nodes:
        next_move: int = move_set[number_of_steps % len(move_set)]
        visited_nodes.append(current_node)
        current_node = node_dict[current_node][next_move]
        number_of_steps += 1

    loop_start: str = current_node
    current_node = start_node
    initial_steps: int = 0
    initial_nodes: list[str] = []
    while current_node != loop_start:
        next_move: int = move_set[initial_steps % len(move_set)]
        initial_nodes.append(current_node)
        current_node = node_dict[current_node][next_move]
        initial_steps += 1

    # loop_nodes: list[str] = visited_nodes[initial_steps:]
    loop_length: int = len(visited_nodes) - initial_steps

    return initial_steps, loop_length


###############################################################################


def calculate_steps_to_zzz(input_data: list[str]) -> int:
    """
    Takes the lines of the input file, applies the moveset beginning with the
    node AAA and follows the path until it reaches the node ZZZ.

    Parameters
    ----------
    input_data: list[str]
        The input file, read linewise.

    Returns
    -------
    int
        The total number of steps from AAA to ZZZ.

    Raises
    ------
    None
    """
    move_set: str = input_data[0]
    move_dict: list[int] = parse_moveset(move_set)
    node_set: list[str] = input_data[2:]
    first_node: str = "AAA"
    last_node: str = "ZZZ"
    node_dict = parse_nodeset(node_set)

    number_of_steps: int = walk_to_exit(
        move_dict, node_dict, first_node, last_node
    )

    return number_of_steps


###############################################################################


def calculate_steps_to_loop(input_data: list[str]) -> int:
    """
    Takes the lines of the input file, applies the moveset beginning with the
    node AAA and follows the path until it reaches a loop.

    Parameters
    ----------
    input_data: list[str]
        The input file, read linewise.

    Returns
    -------
    int
        The total number of steps from AAA to the loop.

    Raises
    ------
    None
    """
    moves: str = input_data[0]
    move_set: list[int] = parse_moveset(moves)
    node_set: list[str] = input_data[2:]
    node_dict: dict[str, list[str]] = parse_nodeset(node_set)
    first_nodes: list[str] = list(filter(lambda x: x[2] == "A", node_dict))

    offset_lengths: list[int] = []
    circle_lengths: list[int] = []
    for node_iter in first_nodes:
        initial_steps: int = 0
        loop_length: int = 0
        initial_steps, loop_length = find_shortest_loop_length(
            move_set, node_dict, node_iter
        )
        offset_lengths.append(initial_steps)
        circle_lengths.append(loop_length)

    circle_lengths.append(len(move_set))
    lowest_common_multiple: int = numpy.lcm.reduce(circle_lengths)

    return lowest_common_multiple


###############################################################################


def main() -> None:
    """
    Runs the whole script.
    """
    # Performance
    time_start: float = time.perf_counter()

    # Parsing of file
    input_data: list[str] = read_file_linewise(os.getcwd() + "/input_day_08")

    # 08-A Sum of extracted numbers:
    print(
        "Number of steps from AAA to ZZZ: "
        + f"{calculate_steps_to_zzz(input_data)}"
    )

    # 08-B Sum of extracted numbers, words and digits:
    print(
        "Number of steps from AAA to loop: "
        + f"{calculate_steps_to_loop(input_data)}"
    )

    # Performance
    time_end: float = time.perf_counter() - time_start
    print(f"{time_end=:.3f}")


main()
