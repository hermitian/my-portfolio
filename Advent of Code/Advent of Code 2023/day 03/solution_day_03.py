"""
Advent of code 2023, day 03
"""
import os
import re
import time

###############################################################################

DIGITS: list[str] = [
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    " ",
]
DIGITS_AND_STAR: list[str] = [
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    " ",
    "*",
]
NON_DIGIT_SYMBOLS: list[str] = [
    "*",
    "/",
    "@",
    "$",
    "#",
    "-",
    "=",
    "+",
    "%",
    "&",
]

###############################################################################


class NumberWithCoords:
    """
    Holds a number and the line/element coordinates where it starts and ends.
    """

    def __init__(
        self,
        value: int,
        coord_line: int,
        coord_row_start: int,
        coord_row_end: int,
    ) -> None:
        """
        Initializes NumberWithCoords with the number's position on the grid.

        Parameters
        ----------
        value: int
            The numerical value of the complete number.
        coord_line: int
            The row coordinate of the number.
        coord_row_start: int
            The column coordinate where the number starts.
        coord_row_end: int
            The column coordinate where the number ends.

        Returns
        -------
        None

        Raises
        ------
        None
        """
        self.value: int = value
        self.coord_line: int = coord_line
        self.coord_row_start: int = coord_row_start
        self.coord_row_end: int = coord_row_end
        self.length: int = coord_row_end - coord_row_start

    ###########################################################################

    def __str__(self) -> str:
        """
        Formatted output of the number's value and coordinates.

        Parameters
        ----------
        None

        Returns
        -------
        str
            The formatted output string.

        Raises
        ------
        None
        """
        return (
            "Line: "
            + str(self.coord_line)
            + " Start: "
            + str(self.coord_row_start)
            + " End: "
            + str(self.coord_row_end)
            + " Value: "
            + str(self.value)
        )

    ###########################################################################

    def __repr__(self) -> str:
        """
        Representation of the number value.

        Parameters
        ----------
        None

        Returns
        -------
        The value of the number as a string.

        Raises
        ------
        None
        """
        return str(self.value)

    ###########################################################################

    def is_at_coords(self, line: int, element: int) -> bool:
        """
        Checks if the given line/element coordinate hits this number instance

        Parameters
        ----------
        line: int
            Row coordinate to check against self.coord_line.
        element: int
            Column coordinate to check against self.coord_row_start.

        Returns
        -------
        True if the stored coordinates match the given (self.coord_line,
        self.coord_row_start).

        Raises
        ------
        None
        """
        if line != self.coord_line:
            return False

        if self.coord_row_end >= element >= self.coord_row_start:
            return True

        return False

    ###########################################################################

    def get_line(self) -> int:
        """
        Returns the line coordinate where the number is.

        Parameters
        ----------
        None

        Returns
        -------
        int
            Row coordinate.

        Raises
        ------
        None
        """
        return self.coord_line

    ###########################################################################

    def get_start(self) -> int:
        """
        Returns the element coordinate where the number starts.

        Parameters
        ----------
        None

        Returns
        -------
        int
            Column coordinate where number starts.

        Raises
        ------
        None
        """
        return self.coord_row_start

    ###########################################################################

    def get_end(self) -> int:
        """
        Returns the element coordinate where the number ends.

        Parameters
        ----------
        None

        Returns
        -------
        int
            Column coordinate where number ends.

        Raises
        ------
        None
        """
        return self.coord_row_end

    ###########################################################################

    def to_int(self) -> int:
        """
        Returns only the value as an int.

        Parameters
        ----------
        None

        Returns
        -------
        int
            Value of the stored number as an int.

        Raises
        ------
        None
        """
        return self.value


###############################################################################


def read_file_as_array(filepath: str) -> list[list[str]]:
    """
    Reads the given file (with absolute path) into a 2d array with each
    character as an element of the inner array.

    Parameters
    ----------
    filepath: str
        The absolute path to the target file.
    head: int, default = 0
        The number of lines to read from the beginning of the file.

    Returns
    -------
    list[list[str]]
        The characters of the file as a list of lists.

    Raises
    ------
    None
    """
    input_data: list[list[str]] = []
    with open(filepath, "r", encoding="utf-8") as input_file:
        for line in input_file:
            line_content: str = line.rstrip()
            line_array: list[str] = list(line_content)
            input_data.append(line_array)

    return input_data


###############################################################################


def check_lines_same_length(input_array: list[list[str]]) -> bool:
    """
    Takes a 2d array and checks if all subarrays are of same length.

    Parameters
    ----------
    input_array: list[list[str]]
        The input file as an array of lines and characters.

    Returns
    -------
    bool
        True if all lines in the array are of same length.

    Raises
    ------
    None
    """
    length = None
    for line in input_array:
        if length is None:
            length = len(line)
        if len(line) != length:
            return False

    return True


###############################################################################


def remove_dots(input_array: list[list[str]]) -> list[list[str]]:
    """
    Takes a 2d array and removes all "." from it, replacing them by an empty
    string. Mostly done for readability during debugging.

    Parameters
    ----------
    input_array: list[list[str]]
        The input file as an array of lines and characters.


    Returns
    -------
    list[list[str]]
        The cleaned up input array.

    Raises
    ------
    None
    """
    input_no_dots: list[list[str]] = []
    for line in input_array:
        line_no_dots: list[str] = []
        for element in line:
            if element == ".":
                line_no_dots.append(" ")
                continue
            line_no_dots.append(element)

        input_no_dots.append(line_no_dots)

    return input_no_dots


###############################################################################


def get_symbol_coords(
    input_no_dots: list[list[str]], symbols: list[str] | None = None
) -> list[tuple[int, int]]:
    """
    Takes a 2d array and outputs a list of tupels, containing the coordinates
    of all symbols.

    Parameters
    ----------
    input_no_dots: list[list[str]]
        The input file as an array of lines and characters, cleaned of all ".".

    Returns
    -------
    list[tuple[int, int]]
        A list of row and column coordinates for each found "*" symbol.

    Raises
    ------
    """
    if symbols is None:
        symbols = NON_DIGIT_SYMBOLS

    symbol_coords: list[tuple[int, int]] = []
    for line_iter, line in enumerate(input_no_dots):
        for element_iter, element in enumerate(line):
            if element in symbols:
                symbol_coords.append((line_iter, element_iter))

    return symbol_coords


###############################################################################


def check_for_number_neighbour(
    input_array: list[list[str]], symbol_coords: tuple[int, int]
) -> bool:
    """
    Takes a 2d array and a tupel of 2d coordinates and checks whether there is
    a digit in any adjacent element.

    Parameters
    ----------
    input_array: list[list[str]]
        The 2d array.
    symbol_coords: tuple[int, int]
        The coordinate tuple to check for neighbors.

    Returns
    -------
    bool
        True if any neighbor is digit.

    Raises
    ------
    None
    """
    symbol_line: int = symbol_coords[0]
    symbol_element: int = symbol_coords[1]

    for line_iter in [-1, 0, 1]:
        target_line: int = symbol_line + line_iter
        if target_line < 0 or target_line >= len(input_array):
            continue

        line: list[str] = input_array[target_line]
        for element_iter in [-1, 0, 1]:
            target_element: int = symbol_element + element_iter
            if target_element < 0 or target_element >= len(line):
                continue

            if line[target_element].isdigit():
                return True

    return False


###############################################################################


def get_relevant_symbol_coords(
    input_array: list[list[str]], symbol_coords: list[tuple[int, int]]
) -> list[tuple[int, int]]:
    """
    Takes a 2d array and a list of tuples with the coordinates of symbols and
    checks whether there is at least one number on an adjacent field.
    Returns the list of tuples with coordinates without neighbouring numbers
    removed.

    Parameters
    ----------
    input_array: list[list[str]]
        The 2d array.
    symbol_coords: list[tuple[int, int]]
        List of coordinates to check for neighbors.

    Returns
    -------
    list[tuple[int, int]]
        A list of coordinates that have digit neighbors.

    Raises
    ------
    None
    """
    symbol_coords_relevant: list[tuple[int, int]] = []
    for symbol in symbol_coords:
        has_number_neighbour: bool = check_for_number_neighbour(
            input_array, symbol
        )
        if has_number_neighbour:
            symbol_coords_relevant.append(symbol)

    return symbol_coords_relevant


###############################################################################


def get_all_numbers(input_array: list[list[str]]) -> list[NumberWithCoords]:
    """
    Takes a 2d array and returns a list of numbers_with_coords with all numbers
    found in the array.

    Parameters
    ----------
    input_array: list[list[str]]
        The 2d array.

    Returns
    -------
    list[NumberWithCoords]
        A list of all found numbers.

    Raises
    ------
    None
    """
    found_numbers: list[NumberWithCoords] = []
    for line_counter, line_iter in enumerate(input_array):
        current_number_digits: list[str] = []
        cursor_on_number: bool = False
        current_number_row_start: int = -1

        for element_counter, element_iter in enumerate(line_iter):
            # Find start of number
            if element_iter.isdigit() and not cursor_on_number:
                cursor_on_number = True
                current_number_row_start = element_counter

            # Extend number
            if cursor_on_number and element_iter.isdigit():
                current_number_digits.append(element_iter)

            # Hit EOL
            if cursor_on_number and element_counter == len(line_iter) - 1:
                cursor_on_number = False

                current_number = int("".join(current_number_digits))
                complete_number: NumberWithCoords = NumberWithCoords(
                    current_number,
                    line_counter,
                    current_number_row_start,
                    element_counter,
                )
                found_numbers.append(complete_number)

                current_number_row_start = -1
                current_number_digits = []
                continue

            # Hit end of number
            if cursor_on_number and (
                not element_iter.isdigit()
                or element_counter == len(line_iter) - 1
            ):
                cursor_on_number = False

                current_number = int("".join(current_number_digits))
                complete_number: NumberWithCoords = NumberWithCoords(
                    current_number,
                    line_counter,
                    current_number_row_start,
                    element_counter - 1,
                )
                found_numbers.append(complete_number)

                current_number_row_start = -1
                current_number_digits = []
                continue

    return found_numbers


###############################################################################


def get_neighbour_coords(
    coord: tuple[int, int], max_row: int, max_column: int
) -> list[tuple[int, int]]:
    """
    Takes a line/element coordinate and returns all (up to 8) valid
    neighbouring coordinates.

    Parameters
    ----------
    coord: tuple[int, int]
        The coordinates for which to calculate all neighboring coordinates.
    max_row: int
        The number of rows in the grid.
    max_column: int
        The number of coumns in the grid.

    Returns
    -------
    list[tuple[int, int]]
        List of 2d coordinates of the neighboring elements.

    Raises
    ------
    """
    row_lookaround: list[int] = [0]
    column_lookaround: list[int] = [0]
    neighbours: list[tuple[int, int]] = []

    # Check if first/last row
    if coord[0] > 0:
        row_lookaround.append(-1)
    if coord[0] < max_row:
        row_lookaround.append(1)

    # Check if first/last column
    if coord[1] > 0:
        column_lookaround.append(-1)
    if coord[1] < max_column:
        column_lookaround.append(1)

    # Iterate possible neighbors
    for row_iter in row_lookaround:
        for column_iter in column_lookaround:
            if not (row_iter == 0 and column_iter == 0):
                neighbours.append(
                    (coord[0] + row_iter, coord[1] + column_iter)
                )

    return neighbours


###############################################################################


def check_number_at_coord(
    coord: tuple[int, int], numbers: list[NumberWithCoords]
) -> tuple[NumberWithCoords | None, list[NumberWithCoords]]:
    """
    Takes a tuple of line/element coordinates and a list of NumberWithCoords
    and returns the NumberWithCoords that is at that coordinate, if any, and
    the list of NumberWithCoords with that one removed.

    Parameters
    ----------
    coord: tuple[int, int]
        The coordinate to check for the presence of a number.
    number: list[NumberWithCoords]
        A list of numbers to check.

    Returns
    -------
    NumberWithCoords
        The found number. If none were found, returns None as first value.
    list[NumberWithCoords]
        The list with all remaining numbers.

    Raises
    ------
    None
    """
    remaining_numbers: list[NumberWithCoords] = []
    found_number: NumberWithCoords | None = None

    for number in numbers:
        if number.is_at_coords(coord[0], coord[1]):
            found_number = number
            continue
        remaining_numbers.append(number)

    return found_number, remaining_numbers


###############################################################################


def calculate_gear_ratios(
    input_array: list[list[str]],
    numbers: list[NumberWithCoords],
    symbols: list[tuple[int, int]],
) -> list[int]:
    """
    Takes a 2d array and a list of found NumberWithCoords and returns the list
    with irrelevant numbers removed, i.e. all numbers that don't have an
    adjacent symbol.

    Parameters
    ----------
    input_array: list[list[str]]
        The 2d array.
    numbers: list[NumberWithCoords]
        A list with all numbers that were found in the grid.
    symbols: list[tuple[int, int]]
        A list of coordinates, where "*" symbols can be found.

    Returns
    -------
    list[int]
        A list of all gear ratios for the gears in the grid.

    Raises
    ------
    None
    """
    numbers_remaining: list[NumberWithCoords] = numbers
    gear_ratios: list[int] = []

    for symbol in symbols:
        neighbouring_numbers: list[NumberWithCoords] = []
        neighbour_coords: list[tuple[int, int]] = get_neighbour_coords(
            symbol, len(input_array) - 1, len(input_array[0]) - 1
        )

        for neighbour_coord in neighbour_coords:
            found_number, numbers_remaining = check_number_at_coord(
                neighbour_coord, numbers_remaining
            )
            if found_number:
                neighbouring_numbers.append(found_number)

        if len(neighbouring_numbers) == 2:
            numbers_as_ints: list[int] = [
                number_iter.to_int() for number_iter in neighbouring_numbers
            ]

            gear_ratio: int = 1
            for number_iter in numbers_as_ints:
                gear_ratio *= number_iter
            gear_ratios.append(gear_ratio)

    return gear_ratios


###############################################################################


def get_relevant_numbers(
    input_array: list[list[str]], part_numbers: list[NumberWithCoords]
) -> list[NumberWithCoords]:
    """
    Takes a 2d array and a list of found numbers and returns only numbers with
    a symbol as its neighbor.

    Parameters
    ----------
    input_array: list[list[str]]
        The 2d array.
    part_numbers: list[NumberWithCoords]
        The list of numbers to filter for those with symbol neighbors.

    Returns
    -------
    list[NumberWithCoords]
        The filtered list.

    Raises
    ------
    None
    """
    relevant_part_numbers: list[NumberWithCoords] = []
    max_column: int = len(input_array[0]) - 1
    max_row: int = len(input_array) - 1

    for number_iter in part_numbers:
        number_coord: tuple[int, int] = (
            number_iter.get_line(),
            number_iter.get_start(),
        )
        number_length: int = number_iter.length

        row_lookaround: list[int] = [0]
        column_lookaround: list[int] = list(range(0, number_length + 1))

        # Check for first/last row collision
        if number_coord[0] > 0:
            row_lookaround.append(-1)
        if number_coord[0] < max_row:
            row_lookaround.append(1)

        # Check for first/last column collision
        if number_coord[1] > 0:
            column_lookaround.append(-1)
        if number_coord[1] + number_iter.length < max_column:
            column_lookaround.append(number_iter.length + 1)

        # Iterate possible neighbors, break if symbol found
        for row_iter in row_lookaround:
            for column_iter in column_lookaround:
                if row_iter == 0 and column_iter in list(
                    range(0, number_length + 1)
                ):
                    continue

                if (
                    input_array[number_coord[0] + row_iter][
                        number_coord[1] + column_iter
                    ]
                    in NON_DIGIT_SYMBOLS
                ):
                    relevant_part_numbers.append(number_iter)

    return relevant_part_numbers


###############################################################################


def get_part_numbers(input_array: list[list[str]]) -> list[int]:
    """
    Takes a 2d array and extracts all valid part numbers.

    Parameters
    ----------
    input_array: list[list[str]]
        The 2d array.

    Returns
    -------
    list[int]
        A list of all valid part numbers.

    Raises
    ------
    None
    """
    input_no_dots: list[list[str]] = remove_dots(input_array)

    part_numbers: list[NumberWithCoords] = get_all_numbers(input_no_dots)

    relevant_part_numbers: list[int] = [
        number.to_int()
        for number in get_relevant_numbers(input_array, part_numbers)
    ]

    return relevant_part_numbers


###############################################################################


def get_gear_ratios(input_array: list[list[str]]) -> list[int]:
    """
    Takes a 2d array and extracts all gear ratios for valid parts.

    Parameters
    ----------
    input_array: list[list[str]]
        The 2d array.

    Returns
    -------
    list[int]
        A list of all gear ratios in the grid.

    Raises
    ------
    None
    """
    input_no_dots: list[list[str]] = remove_dots(input_array)

    symbol_coords: list[tuple[int, int]] = get_symbol_coords(
        input_no_dots, ["*"]
    )
    symbol_coords_relevant: list[tuple[int, int]] = get_relevant_symbol_coords(
        input_no_dots, symbol_coords
    )

    numbers: list[NumberWithCoords] = get_all_numbers(input_array)
    gear_ratios = calculate_gear_ratios(
        input_array, numbers, symbol_coords_relevant
    )

    return gear_ratios


###############################################################################


def main() -> None:
    """
    Runs the whole script.
    """
    # Performance
    time_start: float = time.perf_counter()

    # Parsing of file
    input_data: list[list[str]] = read_file_as_array(
        os.getcwd() + "/input_day_03"
    )

    # 02-A: Sum IDs of possible games
    print(f"Sum of all part numbers: {sum(get_part_numbers(input_data))} ")

    # 02-B Minimum set of needed cubes:
    print(f"Sum of all gear ratios: {sum(get_gear_ratios(input_data))}")

    # Performance
    time_end: float = time.perf_counter() - time_start
    print(f"{time_end=:.3f}")


main()
