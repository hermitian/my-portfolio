"""
Advent of code 2023, day 10
"""
import os
import time

import numpy as np

###############################################################################

# Movement directions: Cardinal directions on a 2d grid. Positive values in
# first position move down/south.
COMPASS_TO_GRID: dict[str, tuple[int, int]] = {
    "S": (1, 0),
    "W": (0, -1),
    "N": (-1, 0),
    "E": (0, 1),
}

###############################################################################


def progress_bar(
    progress: int,
    total: int,
):
    """
    Takes a number of steps performed and a number of total steps and turns
    them into a progress bar from 0% to 100%.

    Parameters
    ----------
    progress: int
        The number of steps already performed.
    total: int
        The total number of steps to be performed.

    Returns
    -------
    None

    Raises
    ------
    None
    """
    percent = 100 * (progress / float(total))
    progress_bar = "█" * int(percent) + "-" * (100 - int(percent))
    if progress >= total:
        print(f"\r|{progress_bar}| {percent:.2f}%", end="\r")
        print("\r")
    else:
        print(f"\r|{progress_bar}| {percent:.2f}%", end="\r")


###############################################################################


def read_file_as_array(filepath: str) -> np.ndarray:
    """
    Reads the given file (with absolute path) into a 2d array with each
    character as an element of the inner array.

    Parameters
    ----------
    filepath: str
        The absolute path to the target file.

    Returns
    -------
    list[list[str]]
        The lines of the file as a list of lists, each character being an
        element of the inner lists.

    Raises
    ------
    None
    """
    input_data: list[list[str]] = []
    with open(filepath, "r", encoding="utf-8") as input_file:
        for line in input_file:
            line_content: str = line.rstrip()
            line_array: list[str] = list(line_content)
            input_data.append(line_array)

    return np.array(input_data)


###############################################################################


def find_all_pipes(grid: np.ndarray) -> list[tuple[int, int]]:
    """
    Takes a 2d-grid and starting coordinates and finds an ordered list of
    coordinates that are connected to the starting point by pipe-like symbols.

    Parameters
    ----------
    grid: np.ndarray
        The grid on which to search.
    start_point: tuple[int, int]
        Coordinates of the starting point.

    Returns
    -------
    list[tuple[int, int]]
        An ordered list of coordinates that belong to the pipe network
        connected to the starting point.

    Raises
    ------
    None
    """

    ###########################################################################
    # Functions/Methods
    ###########################################################################

    def find_first_connection(
        current_coords: tuple[int, int], origin_direction: str
    ) -> str:
        grid_max_x: int = np.shape(grid)[1]
        grid_max_y: int = np.shape(grid)[0]

        if (
            origin_direction != "S"
            and current_coords[0] > 0
            and grid[current_coords] in ["|", "L", "J", "S"]
            and grid[current_coords[0] - 1, current_coords[1]]
            in ["|", "F", "7", "S"]
        ):
            return "N"
        if (
            origin_direction != "W"
            and current_coords[1] < grid_max_x - 1
            and grid[current_coords] in ["-", "F", "L", "S"]
            and grid[current_coords[0], current_coords[1] + 1]
            in ["-", "J", "7", "S"]
        ):
            return "E"
        if (
            origin_direction != "N"
            and current_coords[0] < grid_max_y - 1
            and grid[current_coords] in ["|", "F", "7", "S"]
            and grid[current_coords[0] + 1, current_coords[1]]
            in ["|", "J", "L", "S"]
        ):
            return "S"
        if (
            origin_direction != "E"
            and current_coords[1] > 0
            and grid[current_coords] in ["-", "J", "7", "S"]
            and grid[current_coords[0], current_coords[1] - 1]
            in ["-", "F", "L", "S"]
        ):
            return "W"
        return ""

    ###########################################################################

    def perform_movement(
        current_coords: tuple[int, int], movement: str
    ) -> tuple[int, int]:
        movement_as_tuple: tuple[int, int] = COMPASS_TO_GRID[movement]
        return (
            current_coords[0] + movement_as_tuple[0],
            current_coords[1] + movement_as_tuple[1],
        )

    ###########################################################################

    def find_start_point_coords(
        grid: np.ndarray,
    ) -> tuple[int, int]:
        start_point_coords: tuple = np.where(grid == "S")
        start_point: tuple[int, int] = (
            start_point_coords[0][0],
            start_point_coords[1][0],
        )

        return start_point

    ###########################################################################
    # Execution
    ###########################################################################

    # Start at "S"
    start_point: tuple[int, int] = find_start_point_coords(grid)

    # Follow the only possible path and add to list of connected segments until
    # "S" is reached again.
    path: list[tuple[int, int]] = []
    current_symbol: str = grid[start_point]
    current_coords: tuple[int, int] = start_point
    last_movement: str = ""
    while current_symbol != "S" or len(path) == 0:
        # Get the next direction that allows movement.
        next_movement: str = find_first_connection(
            current_coords, last_movement
        )
        path.append(current_coords)
        # Move one step in that direction.
        current_coords = perform_movement(current_coords, next_movement)
        current_symbol = grid[current_coords]
        last_movement = next_movement

    return path


###############################################################################


def get_area_inside(
    grid: np.ndarray, pipe_loop: list[tuple[int, int]]
) -> int:  # grid: list[list[str]]) -> int:
    """
    Takes a grid and scans it by lines. In each lines it counts the elements
    inside of the *-delimited area. It is assumed, that the counting starts
    on the outside, so the first *
    getting crossed leads to the inside area. Consecutive *s are a line of
    pipes and

    Parameters
    ----------
    grid: np.ndarray
        The grid as an ndarray.
    pipe_loop: list[tuple[int, int]]
        An ordered list of coordinates that belong to the pipe network
        connected to the starting point.

    Returns
    -------
    int
        The area inside the pipe loop.

    Raises
    ------
    None
    """
    # Clean input of all symbols not part of the pipe loop
    grid_cleaned: list[list[str]] = []
    grid_length: int = len(grid)
    print("Cleaning grid of non-connected parts:")
    for line_counter, line_iter in enumerate(grid):
        progress_bar(line_counter + 1, grid_length)
        new_line: list[str] = []
        for element_counter, element_iter in enumerate(line_iter):
            if (line_counter, element_counter) not in pipe_loop:
                new_line.append(" ")
            elif element_iter == "S":
                new_line.append("F")  # Works only for my input, not generally
            else:
                new_line.append(element_iter)
        grid_cleaned.append(new_line)

    # Get area inside loop
    area: int = 0
    for line_counter, line_iter in enumerate(grid_cleaned):
        is_inside: bool = False
        is_on_line: bool = False
        line_started_from_below: bool = False
        line_started_from_above: bool = False
        for element in line_iter:
            # Horizontal line
            if is_on_line and element == "-":
                continue
            if element in ["F", "L"]:
                is_on_line = True
            if element == "F":
                line_started_from_below = True
            if element == "L":
                line_started_from_above = True

            # Vertical lines, continued after parallel shift
            line_parallel_continued: bool = (
                is_on_line and line_started_from_below and element == "J"
            ) or (is_on_line and line_started_from_above and element == "7")
            if line_parallel_continued:
                is_inside = not is_inside

            if is_on_line and element in ["7", "J"]:
                is_on_line = False
                line_started_from_above = False
                line_started_from_below = False
                continue

            # Walls
            if element == "|":
                is_inside = not is_inside
            if is_inside and element == " ":
                area += 1

    return area


###############################################################################


def main() -> None:
    """
    Runs the whole script.
    """
    # Performance
    time_start: float = time.perf_counter()

    # Warning
    print(
        "Warning: This script might not work for all inputs, the relevant "
        + "line is marked with a comment. Function get_area_inside."
    )

    # Parsing of file
    input_data: np.ndarray = read_file_as_array(os.getcwd() + "/input_day_10")

    # 10-A: Find distance of most distant part of connected pipe loop
    pipe_loop: list[tuple[int, int]] = find_all_pipes(input_data)
    print(
        f"Pipe_length: {len(pipe_loop)}, furthest_point: "
        + f"{len(pipe_loop)/2:.0f}"
    )

    # 10-B: Find the area enclosed by the pipe loop
    print(f"Area inside pipe loop: {get_area_inside(input_data, pipe_loop)}")

    # Performance
    time_end: float = time.perf_counter() - time_start
    print(f"{time_end=:.3f}")


main()
