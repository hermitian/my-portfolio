"""
Advent of code 2023, day 15
"""
import os
import time
from typing import TypedDict

###############################################################################


class Lens(TypedDict):
    """A lens has a label and a focal length."""

    lens_label: str
    focal_length: int


class Box(TypedDict):
    """A box has a number and an arbitrary amount of lenses in it."""

    box_number: int
    lens_list: list[Lens]


###############################################################################


def read_file_linewise(filepath: str, head: int = 0) -> list[str]:
    """
    Reads the given file (with absolute path) into a 2d array with each
    character as an element of the inner array.

    Parameters
    ----------
    filepath: str
        The absolute path to the target file.
    head: int, default = 0
        The number of lines to read from the beginning of the file.

    Returns
    -------
    list[str]
        The lines of the file as a list.

    Raises
    ------
    None
    """
    input_data: list[str] = []
    with open(filepath, "r", encoding="utf-8") as input_file:
        line_count: int = 0
        for line in input_file:
            input_data.append(line.rstrip())
            line_count += 1
            if line_count == head:
                break

    return input_data


###############################################################################


def perform_hash_algorithm(entry_sequence: str) -> int:
    """
    Performs the algorithm as given by the problem text.
        # Algorithm:
        # 1) Add ASCII value to total
        # 2) Multiply total with 17
        # 3) Take remainer of total divided by 256
        # 4) Repeat with next letter in entry

    Parameters
    ----------
    entry_sequence: str
        A single entry sequence consisting of letters, followed by an
        "="-sign and an integer or only an "-"-sign

    Returns
    -------
    int
        The resulting number

    Raises
    ------
    None
    """
    total: int = 0
    for letter_iter in entry_sequence:
        total += ord(letter_iter[0])
        total *= 17
        total = total % 256

    return total


###############################################################################


def sum_of_hashes(input_data: str) -> int:
    """
    Takes the complete input string, seperates it into sequences, parses those,
    and then calculates the sum of the partial results.

    Parameters
    ----------
    input_data: str
        A single string of comma-seperated sequences.

    Returns
    -------
    int
        The summed up hash values.

    Raises
    ------
    None
    """
    data_seperated_parts: list[str] = input_data.split(",")
    data_parsed: list[int] = []

    for entry_iter in data_seperated_parts:
        data_parsed.append(perform_hash_algorithm(entry_iter))

    return sum(data_parsed)


###############################################################################


def parse_operation_instructions(
    entry_sequence: str,
) -> dict[str, str | int | Lens]:
    """
    Takes a single sequence and seperates it into lens_label and its hash value
    as box number, the pending operation, and - if necessary - the focal
    length.

    Parameters
    ----------
    entry_sequence: str
        A single entry sequence consisting of letters, followed by an
        "="-sign and an integer or only an "-"-sign

    Returns
    -------
    dict[str, str | int]
        A dictionary with all operation details seperated.

    Raises
    ------
    None
    """
    focal_length: int = 0
    lens_operation: str = ""
    lens_label: str = ""
    if entry_sequence[-1] == "-":
        lens_operation: str = entry_sequence[-1]
        lens_label = entry_sequence[0:-1]
    else:
        focal_length = int(entry_sequence[-1])
        lens_operation: str = entry_sequence[-2]
        lens_label = entry_sequence[0:-2]

    box_number: int = perform_hash_algorithm(lens_label)

    operation_lens: Lens = {
        "lens_label": lens_label,
        "focal_length": focal_length,
    }
    operation_dict: dict[str, str | int | Lens] = {
        "box_number": box_number,
        "operation": lens_operation,
        "lens": operation_lens,
    }

    return operation_dict


###############################################################################


def sort_lens_into_box(target_box: Box, target_lens: Lens) -> Box:
    """
    Takes a target box, a lens label, and a focal length. If the same lens
    label is present in the box, the lens is replaced; if not, a new lens is
    added to the lowest free slot.

    Parameters
    ----------
    target_box: Box
        The box to sort the lens into.
    target_lens: Lens
        The lens to sort into the box.

    Returns
    -------
    Box
        The box dictionary with modified lens list.

    Raises
    ------
    None
    """
    modified_box: Box = {
        "box_number": target_box["box_number"],
        "lens_list": [],
    }

    new_lens: Lens = {
        "lens_label": target_lens["lens_label"],
        "focal_length": target_lens["focal_length"],
    }

    lens_replaced: bool = False
    for lens_iter in target_box["lens_list"]:
        if lens_iter["lens_label"] == target_lens["lens_label"]:
            modified_box["lens_list"].append(new_lens)
            lens_replaced = True
        else:
            modified_box["lens_list"].append(lens_iter)
    if not lens_replaced:
        modified_box["lens_list"].append(new_lens)

    return modified_box


###############################################################################


def remove_lens_from_box(target_box: Box, lens_label: str) -> Box:
    """
    Takes a target box and removes the lens with the given label, if present.

    Parameters
    ----------
    target_box: Box
        The box to remove the lens from.
    lens_label: str
        The letter sequence identifying the lens.

    Returns
    -------
    Box
        The box dictionary with modified lens list.

    Raises
    ------
    None
    """
    modified_box: Box = {
        "box_number": target_box["box_number"],
        "lens_list": [],
    }

    for lens_iter in target_box["lens_list"]:
        if lens_iter["lens_label"] != lens_label:
            modified_box["lens_list"].append(lens_iter)

    return modified_box


###############################################################################


def sum_focal_lengths(box_list: list[Box]) -> int:
    """
    Takes the list of all boxes and returns the total focussing power
    calculated according to the formula:
        (1 + box_number) * lens_slot_from_1 * focal_length

    Parameters
    ----------
    box_list: list[Box]
        The complete list of all 256 boxes.

    Returns
    -------
    int
        The total focussing power

    Raises
    ------
    None
    """
    sum_of_focal_lengths: int = 0
    for box_iter in box_list:
        box_number: int = box_iter["box_number"]
        part_sum: int = 0
        for position, lens_iter in enumerate(box_iter["lens_list"]):
            lens_value: int = (
                (1 + box_number) * (1 + position) * lens_iter["focal_length"]
            )
            part_sum += lens_value
        sum_of_focal_lengths += part_sum
        # print(
        #     f'{box_number=}: {box_iter["lens_list"]=}, {part_sum=}, '
        #     + f"{sum_of_focal_lengths=}"
        # )

    return sum_of_focal_lengths


###############################################################################


def focusing_power(input_data: str) -> int:
    """
    Complete procedure for part 2. Takes the input data, seperates it into a
    list of sequences. These are then turned into a list of parsed operation
    dicts.
    Then, these operations are performed on the list of empty boxes.
    Finally, the total focussing power is calculated.

    Parameters
    ----------
    input_data: str
        A single string of comma-seperated sequences.

    Returns
    -------
    int
        The total focussing power.

    Raises
    ------
    None
    """
    # Seperating and parsing input data
    data_seperated_parts: list[str] = input_data.split(",")
    operation_list: list[dict] = []

    for entry_iter in data_seperated_parts:
        operation_list.append(parse_operation_instructions(entry_iter))

    # Initializing boxes and lens overview
    box_list: list[Box] = []
    for box_iter in range(256):
        box_dict: Box = {
            "box_number": box_iter,
            "lens_list": [],
        }
        box_list.append(box_dict)

    # Apply operation instructions
    for operation_iter in operation_list:
        if operation_iter["operation"] == "=":
            box_list[operation_iter["box_number"]] = sort_lens_into_box(
                box_list[operation_iter["box_number"]],
                operation_iter["lens"],
            )
        if operation_iter["operation"] == "-":
            box_list[operation_iter["box_number"]] = remove_lens_from_box(
                box_list[operation_iter["box_number"]],
                operation_iter["lens"]["lens_label"],
            )

    # Calculate sum of focal lengths
    sum_of_focal_lengths: int = sum_focal_lengths(box_list)

    return sum_of_focal_lengths


###############################################################################


def main() -> None:
    """
    Runs the whole script.
    """
    # Performance
    time_start: float = time.perf_counter()

    # Parsing of file
    input_data: str = read_file_linewise(os.getcwd() + "/input_day_15")[0]

    # 15-A: Find sum of all entries
    print(f"Sum of parsed entries: {sum_of_hashes(input_data)}")

    # 15-B:
    print(f"Focusing power: {focusing_power(input_data)}")

    # Performance
    time_end: float = time.perf_counter() - time_start
    print(f"{time_end=:.3f}")


main()
