This is a growing collection of solutions for the coding challenge "Advent of Code", a yearly event happening in December since 2015.
Unfortunately, I only found out about this in late 2023.
The problems can be found at https://adventofcode.com

While the challenge happens during the time of advent in and around december, there is no time limit to finishing the challenges.
For this reason, this folder does not encompass all challenges. I'm occasionally working on more solutions, and as soon as they are
ready, I will publish them here.
My solutions are not necessarily meant to be short. I use these challenges to improve my coding skills, but also to show my coding
style.

Due to copyright reasons, the puzzle inputs are not part of this upload. Each user gets their own puzzle input and is forbidden from
sharing. If you want to test my code, you will need to create an account with adventofcode.com and get your own puzzle inputs.
If you do so, please store each day's input in the respective folder in a file called "input_day_XX", where XX is the two-digit
challenge day.

I am occasionally working on problems of past years with the goal of solving each year differently, i.e. different ways of structuring
the software, CLI vs. GUI solutions, and different languages.

AoC 2023:
- Python. A single script for each day. Each day sorted into its own folder.
- Day 06: Solved analytically on paper.
- Day 12: Missing
- Day 13: Missing
- Day 14-b: Missing
