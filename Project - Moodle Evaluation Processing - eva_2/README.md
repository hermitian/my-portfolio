eva_2 - A tool for automatically joining moodle evaluation xlsx-files.

- - -
1. [Introduction](#introduction)  
2. [Requirements](#requirements)
3. [Usage](#usage)  

- - -

<a name="introduction">

## 1\. Introduction

Eva_2 is a tool I built when managing several moodle-based online courses for
university-level medicine courses. The courses were structured in a way that
needed a single moodle activity section for each group of participants. For
each of these sections and each course date, an evaluation activity was
presented. These evaluation results were to be combined for each topic
presented, but the moodle evaluation tool did not have an aggregation function.  
This led to over 70 xlsx-files holding the results of each individual evaluation
instance.

This Python package is capable of taking all xlsx-files in a given directory and
combining them, while checking if the evaluation templates fit together.

The results for each given folder is then built into a single pdf-file.

<a name="requirements">

## 2\. Requirements

The package is written in Python 3 and requires a few packages that can be
installed using common Python management tools like pip.

<a name="usage">

## 3\. Usage

After downloading the complete repository and meeting the required Python
packages, a demo can be run by using  
```sh 
python -m eva_2  
```
from the base directory.

Handing over one or several paths as parameters, the package will be run on all
subfolders of the given paths:
```sh
python -m eva_2 path1 path2 ...  
```

Xlsx-files need to be within a subfolder of any given path.
