import sys

from common import eva_2 as eva

if __name__ == "__main__":
    if len(sys.argv) >= 2:
        eva.main(sys.argv[1:])
    else:
        eva.main()
