"""
Tools for building a pdf file from the extracted evaluation results.
"""

import logging
import os
import sys
from textwrap import wrap

import matplotlib.pyplot as plt
from docx import Document
from docx.enum.section import WD_SECTION
from docx.section import Section
from docx.shared import Cm
from docx.table import Table, _Cell, _Row
from docx.text.run import Run
from fpdf import FPDF
from typing_extensions import Doc

###############################################################################


def _plot_mc(q_number: int, q_and_a: dict, results_path: str) -> str:
    """Takes a complete q&a-set and plots all results, saves the plot as a
    single file to results_path."""
    # Logging setup
    logger_root: logging.Logger = logging.getLogger("root")
    logger_root.debug("Entering output._plot_mc")

    # Extract question and list of answers
    question: str = q_and_a["question"]
    answers: list = q_and_a["answers"]

    # Basic plot options
    _, axis = plt.subplots()
    plt.title("\n".join(wrap(question, 60)))
    plt.subplots_adjust(bottom=0.15, top=0.82)

    # Axes: y
    plt.ylabel("#Antworten")
    number_of_options: int = len(answers)
    # TODO funktioniert der generator so??
    y_max: int = max(int(answer_iter[1]) for answer_iter in answers) + 1
    logger_root.info(f"Generator liefert: {y_max=}")
    plt.grid(
        visible=True, which="major", axis="y", linewidth=0.4, color="grey"
    )
    y_ticks = 1
    if y_max > 10:
        y_ticks = 2
    if y_max > 20:
        y_ticks = 5
    if y_max > 50:
        y_ticks = 10
    if y_max > 100:
        y_ticks = 20
    axis.set_yticks(range(0, y_max, y_ticks))
    plt.minorticks_off()
    plt.ylim(0, y_max)

    # Axes: x
    axis.tick_params(axis="x", which="minor", bottom=False)
    plt.xticks(
        range(number_of_options),
        [
            "\n".join(wrap(answer_item[0], int(85 / number_of_options)))
            for answer_item in answers
        ],
        fontsize=7,
    )

    # Plotting
    plt.bar(
        range(number_of_options), [answer_item[1] for answer_item in answers]
    )
    try:
        plot_file = (
            results_path
            + "mc_question_"
            + str(q_number).rjust(3, "0")
            + ".png"
        )
        plt.savefig(plot_file)
    except PermissionError:
        logger_root.error(
            "Could not access results folder " + results_path + ", quitting."
        )
        sys.exit()
    plt.close()

    logger_root.debug("Exiting output._plot_mc")
    return plot_file


###############################################################################


def build_pdf(folder_content: dict) -> None:
    """
    Takes all Qs and As from folder_content and builds a pdf from it.

    Parameters
    ----------
    folder_content: dict
        All parsed, checked, and aggregated file contents.

    Returns
    -------
    None

    Raises
    ------
    None
    """

    # Logging setup
    logger_root: logging.Logger = logging.getLogger("root")
    logger_root.debug(
        "Entering output.build_pdf for path " + str(folder_content["path"])
    )

    # Create directory for plotting and result
    results_path: str = str(folder_content["path"]) + "/results/"
    try:
        os.makedirs(results_path, exist_ok=True)
    except PermissionError:
        logger_root.error(
            "Could not create results folder " + results_path + ", quitting."
        )
        sys.exit()

    # Plot all multiple choice type questions into diagrams
    evaluation_plots: list[str] = []
    for q_counter, q_and_a_iter in enumerate(
        folder_content["evaluation_answers"]
    ):
        if q_and_a_iter["type"] == "mc":
            evaluation_plots.append(
                _plot_mc(q_counter, q_and_a_iter, results_path)
            )

    ###########################################################################

    # Create pdf file
    pdf: FPDF = FPDF()

    # Meta data
    pdf.add_page()
    pdf.set_font("Helvetica", size=20)
    pdf.multi_cell(
        200,
        10,
        txt="\n".join(
            wrap(
                "Auswertung für den Kursnamen "
                + str(folder_content["lecture_name"]),
                40,
            )
        ),
        align="C",
    )
    pdf.set_font("Helvetica", size=12)
    pdf.ln(10)
    pdf.cell(
        w=200,
        h=10,
        txt="Anzahl der abgegebenen Fragebögen: "
        + str(folder_content["evaluation_count"]),
        ln=1,
        align="L",
    )
    pdf.ln(4)

    # Answers to free text questions
    for text_question_iter in [
        text_q_and_a
        for text_q_and_a in folder_content["evaluation_answers"]
        if text_q_and_a["type"] == "text"
    ]:
        pdf.set_font("Helvetica", size=16)
        pdf.multi_cell(180, 10, txt="Frage: " + text_question_iter["question"])
        pdf.ln(2)
        pdf.set_font("Helvetica", size=12)
        for single_answer in text_question_iter["answers"]:
            pdf.multi_cell(180, 8, txt="\t- " + str(single_answer), align="L")
            pdf.ln(1)
        pdf.ln(4)

    # Answers to multiple choice questions
    col_width: float = pdf.w / 2.1
    row_height: float = pdf.h / 3.5
    for plot_counter, mc_answer_plot_iter in enumerate(evaluation_plots):
        if not (plot_counter) % 6:
            pdf.add_page()
        pdf.image(
            mc_answer_plot_iter,
            x=7 + plot_counter % 2 * col_width,
            y=10 + int(plot_counter / 2) % 3 * row_height,
            w=col_width,
        )

    pdf.output(
        name=results_path + str(folder_content["lecture_name"]) + ".pdf"
    )

    logger_root.debug("Exiting output.build_pdf")
    _build_docx(folder_content, evaluation_plots)


def _build_docx(folder_content: dict, evaluation_plots: list[str]) -> None:
    """
    Takes all Qs and As from folder_content and builds a docx from it.

    Parameters
    ----------
    folder_content: dict
        All parsed, checked, and aggregated file contents.
    evaluation_plots: list[str]
        A list of filenames of all plots already created.

    Returns
    -------
    None

    Raises
    ------
    None
    """
    # Logging setup
    logger_root: logging.Logger = logging.getLogger("root")
    logger_root.debug(
        "Entering output.build_docx for path " + str(folder_content["path"])
    )

    # Create docx file
    results_path = str(folder_content["path"]) + "/results/"

    document: Document = Document()
    section: Section = document.sections[-1]
    section.start_type = WD_SECTION.CONTINUOUS
    section.left_margin = Cm(1.7)
    section.right_margin = Cm(1.7)

    # Meta data
    document.add_heading(
        "\n".join(
            wrap(
                "Auswertung für den Kursnamen "
                + str(folder_content["lecture_name"]),
                40,
            )
        ),
        level=0,
    )
    document.add_paragraph(
        "Anzahl der abgegebenen Fragebögen: "
        + str(folder_content["evaluation_count"])
    )

    # Answers to free text questions
    document.add_heading("Freitextfragen und -antworten", level=2)
    if [
        text_q_and_a
        for text_q_and_a in folder_content["evaluation_answers"]
        if text_q_and_a["type"] == "text"
    ]:
        for text_question in [
            text_q_and_a
            for text_q_and_a in folder_content["evaluation_answers"]
            if text_q_and_a["type"] == "text"
        ]:
            paragraph = document.add_paragraph("")
            paragraph.add_run(
                "Frage: " + text_question["question"] + "\n"
            ).bold = True
            for single_answer in text_question["answers"]:
                document.add_paragraph(
                    str(single_answer) + "\n", style="List Bullet"
                )
    else:
        document.add_paragraph("-- Keine --")

    document.add_page_break()

    table: Table = document.add_table(rows=3, cols=2)
    for plot_counter, mc_answer_plot_iter in enumerate(evaluation_plots):

        def add_page(doc, table):
            if plot_counter % 6 == 0 and plot_counter != 0:
                table = doc.add_table(rows=3, cols=2)
            return table

        table = add_page(document, table)
        table_row: _Row = table.rows[int((plot_counter % 6) / 2)]
        table_cell: _Cell = table_row.cells[plot_counter % 2]
        table_run: Run = table_cell.add_paragraph("").add_run()
        table_run.add_picture(mc_answer_plot_iter, width=Cm(8.3))

    document.save(results_path + str(folder_content["lecture_name"]) + ".docx")

    logger_root.debug("Exiting output.build_docx")
