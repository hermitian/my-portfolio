"""
Here all methods for parsing the xlsx-files can be found
"""

import datetime
import logging
import sys
from string import ascii_lowercase

import openpyxl as xl


def parse_file(filename: str) -> dict:
    """
    Loads xlsx-file filename and reads out the contents of the first
    worksheet.

    Parameter
    ---------
    filename: str
        name of an xlsx-file

    Returns
    -------
    dict:
        {meta_data: {filename, date, no. of evalutions, no. of questions},
         entries: [{question, answers:[{text, type, count}, ...]}, ...]
         }
    """
    ###########################################################################
    # Functions/Methods
    ###########################################################################

    def column_name_from_int(column_number: int) -> str:
        """Return the Excel-conforming letter code for a column number."""
        letters: list[str] = [
            letter for _, letter in enumerate(ascii_lowercase, start=1)
        ]
        return letters[column_number - 1]

    ###############################################################################

    def convert_dateformat_german(date: str) -> datetime.date:
        """
        Takes a German date format and returns a corresponding dateobj.
        E.g.: '5. Juli 2013' -> date(2013,07,05)
        """
        logger_root.debug("Entering _convert_dateformat_german")

        # Convert date format into seperate number values for day, month, year.
        MONTHS: list[str] = [
            "Januar",
            "Februar",
            "März",
            "April",
            "Mai",
            "Juni",
            "Juli",
            "August",
            "September",
            "Oktober",
            "November",
            "Dezember",
        ]
        date = date.lstrip()
        date_string: str
        _, date_string, _ = date.split(", ")

        day_string: str
        month_name: str
        year_string: str
        day_string, month_name, year_string = date_string.split(" ")
        day_string = day_string.rstrip(".")

        day: int = int(day_string)
        year: int = int(year_string)

        month: int = 0
        try:
            month = MONTHS.index(month_name) + 1
        except ValueError:
            logger_root.error(
                "The month with name '"
                + str(month_name)
                + "' is not a valid German month"
            )
            sys.exit()

        # Turn into datetime object.
        try:
            date_converted: datetime.date = datetime.date(year, month, day)
        except ValueError:
            logger_root.error(
                "Could not convert date string '" + str(date) + "' to dateobj"
            )
            sys.exit()
        logger_root.debug(
            "Exiting _convert_dateformat_german with converted "
            + "date "
            + str(date_converted)
        )
        return date_converted

    ###########################################################################
    # Execution
    ###########################################################################

    # Loggin setup
    logger_root: logging.Logger = logging.getLogger("root")
    logger_root.debug("Entering parsing.parse_file for file" + str(filename))

    # Load workbook and first worksheet
    workbook: xl.Workbook = xl.load_workbook(filename, data_only=True)
    worksheet = workbook[str(workbook.sheetnames[0])]

    # Meta data: Date of survey end and number of returned surveys
    # Date
    returned_surveys_number: int = 0
    try:
        date_string: str = worksheet["A1"].value
        (
            _,
            _,
            date_string_time,
        ) = date_string.split(",")
        date_of_survey: datetime.date = convert_dateformat_german(date_string)

        # Time
        hours_name: str
        minutes_name: str
        hours_name, minutes_name = date_string_time.split(":")
        hours: int = int(hours_name)
        minutes: int = int(minutes_name)
        time_of_survey: datetime.time = datetime.time(int(hours), int(minutes))
        datetime_of_survey: str = (
            str(date_of_survey) + "T" + str(time_of_survey)
        )
        logger_root.debug("Extracted datetime info: " + datetime_of_survey)

        # Number of returned surveys
        returned_surveys: str
        _, returned_surveys = worksheet["A2"].value.split(":")
        returned_surveys_number: int = int(returned_surveys)
        logger_root.debug(
            "Extracted number of returned surveys: "
            + str(returned_surveys_number)
        )
    except AttributeError:
        logger_root.warning(
            "Could not extract datetime information from survey "
            + "file "
            + str(filename)
            + ", skipping"
        )
        datetime_of_survey = "None"

    # Extraction of questions and answers
    # Questions are only in column 'B'
    # Answers are in columns from 'C' on
    # Last column and row
    x_max: int = worksheet.max_column
    y_max: int = worksheet.max_row
    logger_root.debug("x_max = " + str(x_max) + ", y_max = " + str(y_max))

    current_y: int = 2
    # Find first question, after the title row
    while (
        not (
            worksheet["B" + str(current_y)].value
            and worksheet["B" + str(current_y - 1)].value
        )
        and current_y < 1000
    ):  # Hard limit to prevent infinite loop
        current_y += 1

    # Search for all questions and their answers
    entries: list[dict[str, str | list[str] | list[list[str]]]] = []
    while current_y <= y_max:
        current_entry: dict[str, str | list[str]] = {
            "question": "",
            "type": "",
            "answers": [],
        }
        # Find next question
        next_question_y: int = current_y + 1
        while (
            worksheet["B" + str(next_question_y)].value is None
            and next_question_y <= y_max
        ):
            next_question_y += 1
        current_entry["question"] = worksheet["B" + str(current_y)].value

        question: str = worksheet["B" + str(current_y)].value

        # Find first and last column for this question
        current_x: int = 1
        while (
            worksheet[
                column_name_from_int(current_x) + str(current_y + 1)
            ].value
            is None
            and current_x < x_max
        ):
            current_x += 1
        answer_min_x: int = current_x

        while (
            worksheet[
                column_name_from_int(current_x + 1) + str(current_y + 1)
            ].value
            is not None
        ):
            current_x += 1

        # Check if this last column is 'Mittelwert'
        # or 'mean' or 'average'
        UNWANTED_WORDS: list[str] = [
            "Mittelwert",
            "mean",
            "Mean",
            "average",
            "Average",
        ]
        if (
            worksheet[column_name_from_int(current_x) + str(current_y)].value
            in UNWANTED_WORDS
        ):
            current_x -= 1
        answer_max_x: int = current_x
        current_x = answer_min_x

        logger_root.debug(
            "Question in row "
            + str(current_y)
            + ": "
            + "answer_min_x = "
            + str(answer_min_x)
            + ", answer_max_x = "
            + str(answer_max_x)
        )

        # Check if the current question is a free form question
        # Extract possible answer texts and their counts
        if (
            worksheet[column_name_from_int(current_x) + str(current_y)].value
            is None
        ):
            # Free form answer
            answer_texts: list[str] = []
            while current_y + 1 < next_question_y:
                if worksheet["C" + str(current_y + 1)].value is not None:
                    answer_texts.append(
                        worksheet["C" + str(current_y + 1)].value
                    )
                current_y += 1
            entries.append(
                {"question": question, "type": "text", "answers": answer_texts}
            )
        else:
            # Multiple-Choice-Answer
            answers: list[list[str]] = []
            while current_x <= answer_max_x:
                answers.append(
                    [
                        worksheet[
                            column_name_from_int(current_x) + str(current_y)
                        ].value,
                        worksheet[
                            column_name_from_int(current_x)
                            + str(current_y + 1)
                        ].value,
                    ]
                )
                current_x += 1
            entries.append(
                {"question": question, "type": "mc", "answers": answers}
            )

        current_y = next_question_y

    meta_data: dict[str, str | int] = {
        "date": datetime_of_survey,
        "evaluation_count": returned_surveys_number,
    }

    logger_root.debug("Exiting parsing.parse_file")
    return {"meta_data": meta_data, "entries": entries}
