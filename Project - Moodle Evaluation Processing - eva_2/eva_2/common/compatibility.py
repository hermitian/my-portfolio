########################################################################
# Tools for checking, if all xlsx-files that are to be combined are
# compatible with each other (same number of questions, same answer
# templates, etc.)
########################################################################

import logging


def check_compatibility(content_list: list[dict]) -> bool:
    """
    Takes a list with dicts each holding the contents of one xlsx-file.
    All subsets of the list are check for different kinds of
    compatibility with each other.

    Parameter
    ---------
    content_list: list[dict]
        A list of dicts of formatting
        {meta_data: {filename, date, no. of evalutions, no. of questions},
         entries: [{question, answers:[{text, type, count}, ...]}, ...]

    Return
    ------
    bool:
        True if all files are compatible
        False if at least one file's formatting does not fit at least
        one other file's formatting.

    Raises
    ------
    None
    """

    class FileFormat:
        """To quickly and easily check for equal formatting"""

        def __init__(self):
            self.path: str | None = None
            self.file_name: str | None = None
            self.returned_surveys: int = 0
            self.number_of_questions: int = 0
            self.q_a_collection: list = []

        #######################################################################

        def __repr__(self):
            contents = (
                "\nFile: "
                + str(self.file_name)
                + "\n\tReturned Surveys: "
                + str(self.returned_surveys)
                + "\n\tNumber of questions: "
                + str(self.number_of_questions)
                + "\n\tQuestions and choices: "
            )
            for question in self.q_a_collection:
                contents = contents + "\n\t\t" + str(question)
            return contents

        #######################################################################

        def __eq__(self, other):
            qa_equal: bool = False
            qa_equal: bool = len(self.q_a_collection) == len(
                other.q_a_collection
            )
            for q_and_a_iter in self.q_a_collection:
                if q_and_a_iter["type"] == "mc":
                    qa_equal = q_and_a_iter in other.q_a_collection
            return (
                self.returned_surveys == other.returned_surveys
                and self.number_of_questions == other.number_of_questions
                and qa_equal
            )

    ###########################################################################

    # A single file is always compatible with itself
    if len(content_list) <= 1:
        return True

    # loop through all files
    formatting_list = []
    for file_iter in content_list:
        formatting: FileFormat = FileFormat()
        formatting.path = file_iter["path"]
        formatting.file_name = file_iter["file"]
        formatting.returned_surveys = file_iter["content"]["meta_data"][
            "evaluation_count"
        ]
        q_and_a_s = file_iter["content"]["entries"]
        formatting.number_of_questions = len(q_and_a_s)
        for single_q_and_a in q_and_a_s:
            formatting.q_a_collection.append(
                {
                    "question": single_q_and_a["question"],
                    "type": single_q_and_a["type"],
                    "choices": len(single_q_and_a["answers"]),
                }
            )

        formatting_list.append(formatting)

    compatible: bool = False
    # If each file is compatible with the next, all files are compatible with
    # each other (transitivity)
    for iterator in range(0, len(formatting_list) - 1):
        if formatting_list[iterator] == formatting_list[iterator + 1]:
            compatible = True
            continue

        logging.warning(
            "At least one file in subfolder "
            + str(formatting_list[iterator].path)
            + " is of different format, skipping this "
            + "subfolder."
        )
        logging.warning(
            "File in this subfolder have the following " + "formats:"
        )
        for formatting_iter in formatting_list:
            logging.warning(formatting_iter)
        compatible = False
        break

    return compatible
