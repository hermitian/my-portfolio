"""
This script combines .xlsx-files with evaluation results automatically
created by the moodle used by the Ruhr-Universität Bochum.
"""

import logging
import os
import sys
from typing import TypedDict

import openpyxl as xl

from common import aggregate, compatibility, output, parsing

###############################################################################


class Subdir(TypedDict):
    subdirectory: str
    filelist: list[str]


###############################################################################


def _get_files(folder: str) -> list[Subdir]:
    """
    Reads in all xlsx-files in a given folder (not including its
    subfolders) and checks for the files being empty.

    Parameter
    ---------
    folder: str
        absolute or relative path to a folder

    Returns
    -------
    list[Subdir]:
        a list of all non-empty xlsx-files in the given folder,
        returned including the path information as given in the
        parameter. Each subdir is a TypedDict with keys 'subdirectory' and
        'filelist'.

    Raises
    ------
    None
    """
    logger_root: logging.Logger = logging.getLogger("root")

    logger_root.debug("Entering _get_files")
    # Get list of folders in given root directory, ignoring demo files
    dir_list = [
        folder + entry
        for entry in os.listdir(folder)
        if not os.path.isfile(folder + entry)
    ]
    logger_root.debug(
        "List of subfolders in given directory: " + str(dir_list)
    )

    # if no data folders, print message
    if not dir_list:
        logger_root.error("No subdirectories in given folder, quitting.")
        quit()

    file_list: list[Subdir] = []
    # Check if files in a subdirectory are xlsx
    for subdir in dir_list:
        # Short method to check for empty xlsx-files
        def file_not_empty(name):
            workbook = xl.load_workbook(subdir + "/" + name, data_only=True)
            worksheet = workbook[str(workbook.sheetnames[0])]
            if worksheet.max_column == 1 and worksheet.max_row == 1:
                return False
            return True

        # Populate list with usable files
        files = [
            file
            for file in os.listdir(subdir)
            if str(file).endswith(".xlsx") and file_not_empty(file)
        ]

        if files:
            file_list.append(
                {"subdirectory": str(subdir) + "/", "filelist": files}
            )
        else:
            logger_root.debug(
                "Subfolder " + str(subdir) + " has no usable files."
            )
    logger_root.debug(
        "Created list of xlsx-files in subdirectories: " + str(file_list)
    )

    # Return each folder as a sublist of files
    logger_root.debug("Exiting _get_files")
    return file_list


###############################################################################


def main(*folder_list: str) -> None:
    """
    The package can only be run via this method. All other methods are
    tools for the different parts of this script's work.

    Parameter
    ---------
    *args:
        A series of absolute or relative path names which to check for
        usable files.

    Returns
    -------
    None

    Raises
    ------
    None
    """
    # Logging setup
    logger_root: logging.Logger = logging.getLogger("root")
    logger_root.setLevel(logging.INFO)
    logging.basicConfig(
        format="%(asctime)s %(levelname)s %(filename)s "
        + "%(funcName)s %(message)s",
        datefmt="%Y-%m-%dT%H-%M-%S",
    )

    demo_folder: str = os.path.abspath("./eva_2/demo_data/") + str("/")
    data_folder: str = os.path.abspath("./eva_2/data/") + str("/")
    directories_todo: list[str] = []

    # Check if args contains folders that exist and
    # contains usable files
    if folder_list:
        given_directories: list[str] = [
            os.path.abspath(dirs) + "/" for dirs in folder_list
        ]
        for folder_iter in given_directories:
            # Folder exists and non-empty?
            if not os.path.exists(folder_iter) or not os.listdir(folder_iter):
                logger_root.error(
                    "Given directory "
                    + str(folder_iter)
                    + " is empty or non-existent, "
                    + "ignoring."
                )
            else:
                directories_todo.append(folder_iter)
                logger_root.debug(
                    "Added directory " + str(folder_iter) + " to the list."
                )
    # If no path is given, use default, if data path empty, start demo
    else:
        if [
            item
            for item in os.listdir(data_folder)
            if os.path.isdir(data_folder + item)
        ]:
            directories_todo.append(data_folder)
            logger_root.info(
                "No path given as parameter, using default: "
                + str(data_folder)
            )
        else:
            directories_todo.append(demo_folder)
            logger_root.info("No path given, data folder empty, starting demo")

    # If nothing to do, quit
    if not directories_todo:
        logger_root.error(
            "None of the given directories could be used, " + "quitting."
        )
        sys.exit()

    # Get file lists for each subdirectory excluding demodata
    file_list: list[Subdir] = []
    for directory in directories_todo:
        new_files: list[Subdir] = _get_files(directory)
        if new_files:
            file_list.extend(new_files)
        else:
            logger_root.info(
                "No usable files found in directory "
                + str(directory)
                + ", skipping."
            )
    if not file_list:
        logger_root.error("No usable files found, exiting.")
        sys.exit()

    # Parse all files
    logger_root.debug("Parsing subfolders")
    subfolder_content = []
    for subdir_counter, subdir_iter in enumerate(file_list):
        subfolder_name: str = subdir_iter["subdirectory"]
        sub_file_list: list[str] = subdir_iter["filelist"]
        logger_root.info(
            "Parsing subfolder "
            + str(subdir_counter + 1)
            + " of "
            + str(len(file_list))
            + " named "
            + str(subfolder_name)
            + " containing files "
            + str(sub_file_list)
        )

        # Get file's content
        file_content: list[dict] = []
        lecture_name = input(
            "Bitte Name des Kurses in Pfad "
            + str(subfolder_name)
            + " angeben (Standard: Name des Unterordners): "
        )
        if lecture_name == "":
            lecture_name = os.path.split(os.path.abspath(subfolder_name))[1]
        for file_iter in sub_file_list:
            parsed_content = {
                "lecture_name": lecture_name,
                "path": subfolder_name,
                "file": file_iter,
                "content": parsing.parse_file(subfolder_name + file_iter),
            }
            if parsed_content["content"]:
                file_content.append(parsed_content)

        # Check compatibility of each subdirectories' files
        # with each other
        logger_root.debug("Checking compatibility")
        compatible: bool = compatibility.check_compatibility(file_content)
        if compatible:
            subfolder_content.append(file_content)
            logger_root.info(
                "All files in subfolder "
                + str(subfolder_name)
                + " are compatible with each other."
            )

    if not subfolder_content:
        logger_root.error("No compatible files left, quitting.")
        sys.exit()

    # Combine contents for each set of files
    logger_root.debug("Combining files of each folder")
    combined_results_list: list[dict] = []
    for subdir_iter in subfolder_content:
        combined_results_list.append(
            aggregate.combine_parsed_surveys(subdir_iter)
        )

    # Plot data
    logger_root.debug("Plotting and creating pdf")
    for subdir_results_iter in combined_results_list:
        output.build_pdf(subdir_results_iter)

    logger_root.debug("Finished.")
