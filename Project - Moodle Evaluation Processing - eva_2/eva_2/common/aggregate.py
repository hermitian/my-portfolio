"""
Module handles the combination of parsing results from different survey files
"""

import logging


def combine_parsed_surveys(content_from_parsing: list) -> dict:
    """
    Combines the elements of different parsed files into one large list

    Arguments
    ---------
    content_from_parsing: list
        A list of outputs from parsing each provided file

    Returns
    -------
    combined_total: dict
        A single list containing the combined parsing results

    Raises
    ------
    None
    """

    ###########################################################################
    # Functions/Methods
    ###########################################################################

    def append_q_and_a_from_survey(
        single_survey: dict, questions_aggregate: list
    ) -> list:
        questions_new: list = questions_aggregate
        list_of_question_strings: list = [
            question_iter["question"] for question_iter in questions_new
        ]

        question_already_aggregated: bool = (
            single_survey["question"] in list_of_question_strings
        )

        if not question_already_aggregated:
            questions_new.append(single_survey)

            logger_root.debug(
                "Added new question single_survey to "
                + "questions_aggregate: %s",
                single_survey["question"],
            )
        else:
            logger_root.debug(
                "Appending answer values to existing question: %s",
                single_survey["question"],
            )

            index: int = [
                questions_iter["question"] for questions_iter in questions_new
            ].index(single_survey["question"])

            # mc
            if single_survey["type"] == "mc":
                # Cycle through all answer choices
                for answer_counter, answer_iter in enumerate(
                    single_survey["answers"]
                ):
                    questions_new[index]["answers"][answer_counter][
                        1
                    ] += answer_iter[1]
                logger_root.debug(
                    "Appended mc-answer count to existing answer."
                    + "\n\tQuestion: %s "
                    + "\n\tAnswer now: %s",
                    questions_new[index]["question"],
                    questions_new[index]["answers"],
                )
            # Free text
            else:
                for text_single_survey_iter in single_survey["answers"]:
                    questions_new[index]["answers"].append(
                        text_single_survey_iter
                    )
                logger_root.debug(
                    "Appended text-answer to existing list of text "
                    + "entries."
                    + "\n\tQuestion: %s"
                    + "\n\tAnswers now: %s",
                    questions_new[index]["question"],
                    questions_new[index]["answers"],
                )
        return questions_new

    ###########################################################################
    # Execution
    ###########################################################################

    # Loggin setup
    logger_root: logging.Logger = logging.getLogger("root")
    logger_root.debug("Entering aggregate.survey_combine")

    # Combine parsed file contents
    pure_content: list = [
        list_item["content"] for list_item in content_from_parsing
    ]

    combined_survey_count: int = 0
    for entry in pure_content:
        combined_survey_count += entry["meta_data"]["evaluation_count"]

    # Answers don't need seperations by files anymore
    pure_content: list = [list_item["entries"] for list_item in pure_content]
    pure_content: list = [
        q_and_a for list_item in pure_content for q_and_a in list_item
    ]

    questions_aggregate: list = []
    for entry_iter in pure_content:
        questions_aggregate = append_q_and_a_from_survey(
            entry_iter, questions_aggregate
        )

    combined_total: dict = {
        "lecture_name": content_from_parsing[0]["lecture_name"],
        "path": content_from_parsing[0]["path"],
        "evaluation_count": combined_survey_count,
        "evaluation_answers": questions_aggregate,
    }

    logger_root.debug("Exiting aggregate.survey_combine")

    return combined_total
