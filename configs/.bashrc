# ~/.bashrc

###############################################################################

# Checks for the shell being interactive or not.
# See https://unix.stackexchange.com/a/257613 for an explanation
[[ $- != *i* ]] && return

###############################################################################

# Aliases
# For downloading video and audio from youtube or other platforms
OUTPUT_PATH_AUDIO="<PATH_TO_YOUR_AUDIO_DIRECTORY>"
alias youtube-audio='yt-dlp -i --output "${OUTPUT_PATH_AUDIO}/%(playlist_title)s/%(channel)s - %(title)s.%(ext)s" --extract-audio --audio-format mp3'
OUTPUT_PATH_VIDEO="<PATH_TO_YOUR_VIDEO_DIRECTORY>"
alias youtube-vid='yt-dlp -i --output "${OUTPUT_PATH_VIDEO}/%(playlist_title)s/%(channel)s - %(title)s.%(ext)s"'

# Git shortcuts
alias gits='git status'
alias gita='git add .'
alias glog='git log --all --decorate --oneline --graph'
alias gitc='git commit'
alias gitp='git push'

# Replacements of often used commands to include certain parameters
alias grep='grep --colour=auto'
alias df='df -h'  # human-readable
alias ls='ls -lish --color=auto'  # List output, index number of files, size, human-readable
alias lsa='ls -lisah --color=auto'  # Show hidden files, too
alias fuck='sudo !!'  # run previous command with sudo
alias dd='dd status=progress'  # statusbar
# Set verbose mode for different commands
alias chmod='chmod -v'
alias chown='chown -v'
alias mkdir='mkdir -v'
alias mount='mount -v'
alias cp='cp -iv'  # interactive
alias mv='mv -iv'  # interactive
alias rm='rm -Iv'  # prompt only once
alias rmdir='rmdir -v'
alias umount='umount -v'

# Tools
alias passgen='/dev/urandom tr -dc _A-Z-a-z-0-9!"§$%&=? | head -c${1:-32};echo;'

###############################################################################

# Bash internal config

# From the Linux Mint .bashrc
# Set colorful PS1 only on colorful terminals.
# dircolors --print-database uses its own built-in database
# instead of using /etc/DIR_COLORS.  Try to use the external file
# first to take advantage of user additions.  Use internal bash
# globbing instead of external grep binary.
safe_term=${TERM//[^[:alnum:]]/?}   # sanitize TERM

match_lhs=""
[[ -f ~/.dir_colors   ]] && match_lhs="${match_lhs}$(<~/.dir_colors)"
[[ -f /etc/DIR_COLORS ]] && match_lhs="${match_lhs}$(</etc/DIR_COLORS)"
[[ -z ${match_lhs}    ]] \
	&& type -P dircolors >/dev/null \
	&& match_lhs=$(dircolors --print-database)
[[ $'\n'${match_lhs} == *$'\n'"TERM "${safe_term}* ]] && use_color=true

if ${use_color} ; then
	# Enable colors for ls, etc.  Prefer ~/.dir_colors #64489
	if type -P dircolors >/dev/null ; then
		if [[ -f ~/.dir_colors ]] ; then
			eval $(dircolors -b ~/.dir_colors)
		elif [[ -f /etc/DIR_COLORS ]] ; then
			eval $(dircolors -b /etc/DIR_COLORS)
		fi
	fi

	if [[ ${EUID} == 0 ]] ; then
		PS1='\[\033[01;31m\][\h\[\033[01;36m\] \W\[\033[01;31m\]]\$\[\033[00m\] '
	else
		PS1='\[\033[01;32m\][\u@\h\[\033[01;37m\] \W\[\033[01;32m\]]\$\[\033[00m\] '
	fi

else
	if [[ ${EUID} == 0 ]] ; then
		# show root@ when we don't have colors
		PS1='\u@\h \W \$ '
	else
		PS1='\u@\h \w \$ '
	fi
fi

# Cleanup
unset use_color safe_term match_lhs sh

# Allow connections to X11 from root
xhost +local:root > /dev/null 2>&1

# For autocompletion
complete -cf sudo

###############################################################################

# Extra PATH infos
export PATH="$HOME/.local/bin:$PATH"

###############################################################################

# Command/Program specific configs

# Python virtualenvwrapper
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/proggen
if [ -f /usr/bin/virtualenvwrapper.sh ]; then
	source /usr/bin/virtualenvwrapper.sh
elif [ -f /usr/local/bin/virtualenvwrapper.sh ]; then
	source /usr/bin/virtualenvwrapper.sh
elif [ -f ~/.local/bin/virtualenvwrapper.sh ]; then
	source ~/.local/bin/virtualenvwrapper.sh
else
	echo "ERROR: No virtualenv wrapper found."
fi

# Enable history appending instead of overwriting.
shopt -s histappend

# If bash_completion is accessible, apply it
[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion

###############################################################################

# Small scripts

# ex - archive extractor
# usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1     ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}
